//
//  DDType.h
//  paixiu
//
//  Created by darkdong on 12-4-5.
//  Copyright (c) 2012年 PaiXiu. All rights reserved.
//

#ifndef paixiu_DDType_h
#define paixiu_DDType_h

#import <CoreLocation/CoreLocation.h>

@class LTPlacemarkModel;

typedef unsigned long long DDUInt64;

typedef void (^DDBlockVoid)(void);
typedef void (^DDBlockBool)(BOOL ok);
typedef void (^DDBlockImage)(UIImage *image);
typedef void (^DDBlockArray)(NSArray *array);
typedef void (^DDBlockCoordinate2D)(CLLocationCoordinate2D revisedCoordinate);
typedef void (^DDBlockObject)(id obj);
typedef void (^DDBlockData)(NSData *data);
typedef void (^DDBlockProgress)(float progress);
typedef void (^DDBlockPlacemark)(LTPlacemarkModel *placemark);
typedef void (^DDBlockError)(NSError *error);
typedef void (^DDBlockBoolAndObj)(BOOL ok,id obj);
#endif
