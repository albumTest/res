//
//  TAFullViewController.h
//  TravelAlbum
//
//  Created by Static Ga on 13-2-6.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TAFullViewController : UIViewController
- (id)initWithPhotos :(NSArray *)photos;
@end
