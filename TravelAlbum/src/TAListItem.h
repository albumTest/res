//
//  TAListItem.h
//  TravelAlbum
//
//  Created by rumble on 13-1-28.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TAListItem : NSObject
@property (nonatomic, strong) NSURL *coverViewURL;
@property (nonatomic, strong) NSString *cityName;
@property (nonatomic) NSInteger photosCount;
@property (nonatomic, strong) NSMutableArray *urlArray;

@property (nonatomic, strong) NSMutableArray *assetsArray;

@end
