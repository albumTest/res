//
//  TAThumbsTableViewController.m
//  TravelAlbum
//
//  Created by Static Ga on 13-2-5.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import "TAThumbsTableViewController.h"
#import "UIView+Helper.h"
#import "TAThumbsTableCell.h"
#import "KTPhotoScrollViewController.h"
#import "KTPhotoView.h"
#import "TATool.h"
#import "KTPhotoBrowserDataSource.h"
#import "KTThumbView.h"
#import "TAFullViewController.h"
#import "TAUtility.h"
#import "TAAppDelegate.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "TAConstantDefine.h"
#import "UINavigationBar+BackgroundImage.h"

@interface TAThumbsTableViewController ()<TAThumbViewSelectedDelegate,KTPhotoBrowserDataSource>
@property (nonatomic, strong) NSArray *thumbsArray;
@property (nonatomic, strong) NSString *barTitle;
@property (nonatomic, strong) NSArray *urlArray;
@end

@implementation TAThumbsTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData:) name:kNotificationAssetsDicFinished object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (id)initWithThumbs:(NSArray *)thumbs title:(NSString *)topTitle {
    if (self = [super init]) {
        self.urlArray = thumbs;
        self.barTitle = topTitle;
    }
    return self;
}
- (void)refreshData :(NSNotification *)aNotification {
    if (0 == self.thumbsArray.count || self.thumbsArray.count != self.urlArray.count) {
        NSDictionary *globalDic = GetAppDelegate().assetURL2assetDic;
        NSMutableArray *tempAssetArray = [NSMutableArray array];
        if (self.urlArray) {
            for (NSURL *url in self.urlArray) {
                ALAsset *asset = [globalDic objectForKey:url];
                if (asset) {
                    [tempAssetArray addObject:asset];
                }
            }
        }
        
        self.thumbsArray = [NSArray arrayWithArray:tempAssetArray];
        [thumbTableView reloadData];
        [self setTableViewFootView];
    }
}

- (void)loadView {
    [super loadView];
    
    self.view.backgroundColor = [UIColor clearColor];
    self.view.frame = [UIScreen mainScreen].bounds;
}

- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont boldSystemFontOfSize:20.0];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = RGBCOLOR(190, 190, 190);
        
        self.navigationItem.titleView = titleView;
    }
    titleView.text = title;
    [titleView sizeToFit];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSDictionary *globalDic = GetAppDelegate().assetURL2assetDic;
    NSMutableArray *tempAssetArray = [NSMutableArray array];
    if (self.urlArray) {
        for (NSURL *url in self.urlArray) {
            ALAsset *asset = [globalDic objectForKey:url];
            if (asset) {
                [tempAssetArray addObject:asset];
            }
        }
    }
    
    self.thumbsArray = [NSArray arrayWithArray:tempAssetArray];
    
	// Do any additional setup after loading the view.
    
    thumbTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height - self.navigationController.navigationBar.height) style:UITableViewStylePlain];
    thumbTableView.backgroundColor = [UIColor whiteColor];
    thumbTableView.delegate = self;
    thumbTableView.dataSource = self;
    thumbTableView.rowHeight = 105;
    thumbTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [thumbTableView reloadData];
    [self setTableViewFootView];
    [self.view addSubview:thumbTableView];
    [self setTitle:self.barTitle];
    
    //    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]
    //
    //                                     initWithTitle:@"旅行相册"
    //
    //                                     style:UIBarButtonItemStylePlain
    //
    //                                     target:self
    //
    //                                     action:nil];
    //    self.navigationItem.backBarButtonItem = cancelButton;
    
    
    UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setBackgroundImage:[[UIImage imageNamed:@"btn_back_grey_normal"] stretchableImageWithLeftCapWidth:15 topCapHeight:0] forState:UIControlStateNormal];
    [backButton setTitle:TALocalizable(@"Travel Photos") forState:UIControlStateNormal];
    [backButton setTitleColor:RGBCOLOR(190, 190, 190) forState:UIControlStateNormal];
    backButton.titleLabel.font=[UIFont boldSystemFontOfSize:12];
    [backButton.titleLabel sizeToFit];
    [backButton sizeToFit];
    backButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
//    backButton.width += 15;
    backButton.width += 20;
    [backButton addTarget:self action:@selector(doBack:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* cancelButtons= [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = cancelButtons;
}

- (void)doBack: (id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [[UIApplication sharedApplication] setStatusBarHidden:NO];
//    
//    UINavigationBar *navbar = [[self navigationController] navigationBar];
//    [navbar setTranslucent:NO];
    
    [self.navigationController.navigationBar setBackGroundImage:@"bg_top"];

    
}

- (BOOL)shouldAutorotate {
    NSLog(@"shouldAutorotate");
    return YES;
}

-(NSInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
    
}

- (void)setTableViewFootView {
    UIView *footView = [[UIView alloc] init];
    footView.height = 70;
    footView.backgroundColor = [UIColor clearColor];
    footView.width = thumbTableView.width;
    
    UILabel *footlabel = [[UILabel alloc] init];
    footlabel.font = [UIFont systemFontOfSize:20];
    footlabel.text = [NSString stringWithFormat:TALocalizable(@"%i Photos"),self.thumbsArray.count];
    [footlabel sizeToFit];
    footlabel.textColor = RGBCOLOR(190, 190, 190);
    footlabel.centerX = footView.width/2;
    footlabel.centerY = footView.height/2;
    footlabel.centerY -= 10;
    [footView addSubview:footlabel];
    thumbTableView.tableFooterView = footView;
    NSInteger rowCount = 0;
    if (self.thumbsArray && self.thumbsArray.count > 0) {
        NSInteger arrayCount = [self.thumbsArray count];
        if(0 == arrayCount % 3){
            rowCount =  arrayCount/3;
        }else {
            rowCount = arrayCount/3 + 1;
        }
    }
    if (rowCount > 1) {
        [thumbTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:rowCount - 1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - tableViewDelegate & dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger rowCount = 0;
    if (self.thumbsArray && self.thumbsArray.count > 0) {
        NSInteger arrayCount = [self.thumbsArray count];
        if(0 == arrayCount % 3){
            rowCount =  arrayCount/3;
        }else {
            rowCount = arrayCount/3 + 1;
        }
    }
    return rowCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"thumbsCellIdentifier";
    TAThumbsTableCell *cell = (TAThumbsTableCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[TAThumbsTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.delegate = self;
    }
    if (self.thumbsArray && self.thumbsArray.count > 0) {
        
        NSUInteger location = indexPath.row * 3;
        NSUInteger diff = self.thumbsArray.count - location;
        NSUInteger length = diff > 3? 3: diff;
        
        [cell setCellWithModels:[self.thumbsArray subarrayWithRange:NSMakeRange(location, length)]];
    }
    return cell;
}

#pragma mark -
#pragma mark - tableViewCellDelegate
- (void)thumbViewSeleceted:(ALAsset *)asset {
    NSInteger index = [self.thumbsArray indexOfObject:asset];
    
    //    TAFullViewController *cc = [[TAFullViewController alloc] initWithPhotos:self.thumbsArray];
    
    KTPhotoScrollViewController *newController = [[KTPhotoScrollViewController alloc]
                                                  initWithDataSource:self
                                                  andStartWithPhotoAtIndex:index andBackTitle:self.barTitle];
    
    [self.navigationController pushViewController:newController animated:YES];
}

#pragma mark -
#pragma mark - KTPhotoScrollViewDelegate
- (NSInteger)numberOfPhotos {
    return self.thumbsArray.count;
}

- (void)imageAtIndex:(NSInteger)index photoView:(KTPhotoView *)photoView {
    __block TAThumbsTableViewController *weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @autoreleasepool {
            ALAsset *asset = [weakSelf.thumbsArray objectAtIndex:index];
            ALAssetRepresentation *representation = [asset defaultRepresentation];
            CGImageRef imageReference = CGImageRetain([representation fullScreenImage]);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIImage *image = [UIImage imageWithCGImage:imageReference];
                [photoView setImage:image];
                CGImageRelease(imageReference);
            });
        }
    });
    //    ALAsset *asset = [self.thumbsArray objectAtIndex:index];
    //    ALAssetRepresentation *representation = [asset defaultRepresentation];
    //    CGImageRef imageReference = [representation fullScreenImage];
    //    UIImage *image = [UIImage imageWithCGImage:imageReference];
    //    [photoView setImage:image];
    //  return;
    /*
     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
     NSURL *url = [self.thumbsArray objectAtIndex:index];
     [[TATool assetsLibrary] assetForURL:url resultBlock:^(ALAsset *asset) {
     @autoreleasepool {
     ALAssetRepresentation *representation = [asset defaultRepresentation];
     CGImageRef imageReference = CGImageRetain([representation fullScreenImage]);
     dispatch_async(dispatch_get_main_queue(), ^{
     UIImage *image = [UIImage imageWithCGImage:imageReference];
     [photoView setImage:image];
     CGImageRelease(imageReference);
     });
     }
     } failureBlock:^(NSError *error) {
     NSLog(@"get image failed");
     }];
     });
     */
}

- (void)thumbImageAtIndex:(NSInteger)index thumbView:(KTThumbView *)thumbView {
    /*
     __block TAThumbsTableViewController *weakSelf = self;
     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
     NSURL *url = [weakSelf.thumbsArray objectAtIndex:index];
     [[TATool assetsLibrary] assetForURL:url resultBlock:^(ALAsset *asset) {
     dispatch_async(dispatch_get_main_queue(), ^{
     [thumbView setThumbImage:[UIImage imageWithCGImage:asset.thumbnail]];
     });
     } failureBlock:^(NSError *error) {
     NSLog(@"get image failed");
     }];
     });
     */
    
}

@end
