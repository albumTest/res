//
//  DDRequiresARC.h
//  paixiu
//
//  Created by darkdong on 12-10-16.
//  Copyright (c) 2012年 PaiXiu. All rights reserved.
//

#ifndef paixiu_DDRequiresARC_h
#define paixiu_DDRequiresARC_h

#if !defined(__has_feature) || !__has_feature(objc_arc)
#error "This file requires ARC support."
#endif


#endif
