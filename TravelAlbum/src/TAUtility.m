//
//  TAUtility.m
//  TravelAlbum
//
//  Created by rumble on 13-1-29.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import "TAUtility.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "TAConstantDefine.h"
#import "JSONKit.h"
#import "TAAppDelegate.h"
#import "TATool.h"
#import "ZipArchive.h"
#import "FMDatabaseQueue.h"
#import "DDType.h"
#import "NSObject+Helper.h"
#import "TAPlaceMarkModel.h"
#import "AFJSONRequestOperation.h"
#import "SVPlacemark.h"

TAAppDelegate *GetAppDelegate(void)
{
    return (TAAppDelegate *)[[UIApplication sharedApplication] delegate];
}

#pragma mark -TravelAlbum DB
FMDatabase *GetCityDB(void) {
#if 1
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *path = [docsPath stringByAppendingPathComponent:@"TravelAlbum.db"];
#else
    NSString *path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"TravelAlbum.db"];
#endif
    return [FMDatabase databaseWithPath:path];
}

void EnumerateAllPhotosToDB(void) {
    dispatch_async([TATool cityQueue], ^{
        GetAppDelegate().isEnumerateAllPhotos = YES;
        NSMutableArray *userDBAssetURLsArray = [NSMutableArray array];
        NSMutableArray *saveToUserArray = [NSMutableArray array];
        FMDatabase *userDB = [TATool sharedUserDB];
        [userDB open];
        //得到用户数据库保存的AssetURL
        NSString *sql = [NSString stringWithFormat:@"SELECT * FROM %@", DBCity2AssetURLTable];
        FMResultSet *rs = [userDB executeQuery:sql];
        while ([rs next]) {
            NSString *assetURL = [rs objectForColumnName:DBPhotoAssetURL];
            [userDBAssetURLsArray addObject:assetURL];
        }
        [rs close];
        FMDatabase *taDB = [TATool sharedCoord2CityNameDB];
        //将必要的数据载入内存
        dispatch_async([TATool dbQueue], ^{
            [taDB open];
            NSLog(@"taDB start %f",[[NSDate date] timeIntervalSince1970]);
            if (0 == GetAppDelegate().cityid2NameMapDic.count) {
                GetAppDelegate().cityid2NameMapDic = DBReadCityNameByID(taDB);
            }
            if (0 == GetAppDelegate().locationTocityIdDic.count) {
                GetAppDelegate().locationTocityIdDic = DBReadCityIDByLocation(taDB);
            }
            NSLog(@"cityid2NameMapDic end %f",[[NSDate date] timeIntervalSince1970]);
        });
        
        //开始读取相册
        NSLog(@"photos start %f",[[NSDate date] timeIntervalSince1970]);
        [[TATool assetsLibrary] enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationReadSuccess object:nil];
                GetAppDelegate().isLocationServerDown = NO;
            });
            //读取相册结束
            if (!group) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAssetsDicFinished object:nil];
                *stop = YES;
                NSLog(@"photos end %f",[[NSDate date] timeIntervalSince1970]);
                dispatch_async([TATool dbQueue], ^{
                    [taDB close];
                    NSLog(@"taDB end %f",[[NSDate date] timeIntervalSince1970]);
                    //修改用户数据库中的数据，关闭数据库
                    TAEndEnumeratePhotosWithWillChangedPhotos(saveToUserArray, userDBAssetURLsArray);
                    GetAppDelegate().isEnumerateAllPhotos = NO;
                    //处理仍然没有分类的照片
                    TAProcessUncategorizedPhotos();
                });
                return;
            }
            if (ALAssetsGroupPhotoStream != [[group valueForProperty:ALAssetsGroupPropertyType] integerValue]) {
                [group setAssetsFilter:[ALAssetsFilter allPhotos]];
                [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *innerStop) {
                    if (!result) {
                        *innerStop = YES;
                        return;
                    }
                    
                    NSDictionary *assetURLs = [result valueForProperty:ALAssetPropertyURLs];
                    NSURL *assetURL = nil;
                    NSUInteger assetCounter = 0;
                    NSNumber *photoDate = [NSNumber numberWithInteger:[[result valueForProperty:ALAssetPropertyDate] timeIntervalSince1970]];
                    if (assetURLs.count > 1) {
                        NSLog(@"assetURLs is %@", assetURLs);
                    }
                    for (NSString *assetURLKey in assetURLs) {
                        assetCounter++;
                        assetURL = [assetURLs valueForKey:assetURLKey];
                        if (assetURL) {
                            break;
                        }
                    }
                    //创建dic assetURL->asset
                    [GetAppDelegate().assetURL2assetDic setObject:result forKey:assetURL];
                    //获取删除照片的assetURL
                    if (NSNotFound !=[userDBAssetURLsArray indexOfObject:assetURL.absoluteString]) {
                        [userDBAssetURLsArray removeObject:assetURL.absoluteString];
                        return;
                    }
                    @autoreleasepool {
                        //获取照片地理位置坐标，从照片的详细信息里读取
                        NSDictionary *metadata = result.defaultRepresentation.metadata;
                        if (metadata && [metadata valueForKey:@"{GPS}"]) {
                            CLLocationCoordinate2D coordinate;
                            coordinate.latitude = [[metadata valueForKeyPath:@"{GPS}.Latitude"] doubleValue];
                            coordinate.longitude = [[metadata valueForKeyPath:@"{GPS}.Longitude"] doubleValue];
                            NSString *latitudeRef = [metadata valueForKeyPath:@"{GPS}.LatitudeRef"];
                            if ([latitudeRef isEqualToString:@"S"]) {
                                coordinate.latitude = -coordinate.latitude;
                            }
                            NSString *longitudeRef = [metadata valueForKeyPath:@"{GPS}.LongitudeRef"];
                            if ([longitudeRef isEqualToString:@"W"]) {
                                coordinate.longitude = -coordinate.longitude;
                            }
                            
                            dispatch_async([TATool dbQueue], ^{
                                //通过坐标得到城市（用自己的数据库，好处就是可以离线）
                                NSDictionary *dic = TAQueryCityNameByLocation(coordinate, taDB);
                                //NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:DBUncategorizedCityName, DBCityName, [NSNumber numberWithInteger:DBUncategorizedCityID], DBCityID, nil];
                                dispatch_async([TATool saveToUserDBQueue], ^{
                                    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:assetURL.absoluteString, DBPhotoAssetURL, photoDate, DBphotoCreateDate,nil];
                                    [dictionary addEntriesFromDictionary:dic];
                                    [saveToUserArray addObject:dictionary];
                                    //发送通知，准备添加信息、刷新页面
                                    dispatch_async(TAGetGlobalQueue(), ^{
                                        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationGetCityName object:dictionary userInfo:nil];
                                    });
                                });
                            });
                        } else {
                            
                        }
                    }
                }];
            }
        } failureBlock:^(NSError *error) {
            NSLog(@"failed");
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationReadPhotoFailed object:nil];
                GetAppDelegate().isLocationServerDown = YES;
            });
        }];
    });

}

NSDictionary *TAQueryCityNameByLocation(CLLocationCoordinate2D coordinate, FMDatabase *db) {
    //NSLog(@"TAQuery start %f", [[NSDate date] timeIntervalSince1970]);
    if (coordinate.latitude < 0.0 || coordinate.latitude > 50.0 || coordinate.longitude < -150.0 || coordinate.longitude > -50.0) {
        //NSLog(@"outside!");
        NSInteger lat_int = (coordinate.latitude * 1000);
        NSInteger lng_int = (coordinate.longitude * 1000);
        NSDictionary *noFoundDic = [NSDictionary dictionaryWithObjectsAndKeys:DBUncategorizedCityName, DBCityName, [NSNumber numberWithInteger:DBUncategorizedCityID], DBCityID, nil];
        for (int i = -1; i < 1; i++) {
            for (int j = -1; j < 1; j++) {
                @autoreleasepool {
                    NSInteger lat = lat_int + i;
                    NSInteger lng = lng_int + j;
                    NSMutableDictionary *subDic = [GetAppDelegate().nearbyLocationToCityMap objectForKey:[NSString stringWithFormat:@"%i,%i", lat, lng]];
                    if (!subDic) {
                        [GetAppDelegate().nearbyLocationToCityMap setObject:noFoundDic forKey:[NSString stringWithFormat:@"%i,%i", lat, lng]];
                    }
                }
            }
        }
        return noFoundDic;
    }

    NSMutableArray *cityIDArray = [NSMutableArray array];
    NSMutableArray *filterIDArray = [NSMutableArray array];
    
    NSInteger lat_int = (coordinate.latitude * 1000);
    NSInteger lng_int = (coordinate.longitude * 1000);
    NSMutableDictionary *cityDic = [GetAppDelegate().nearbyLocationToCityMap objectForKey:[NSString stringWithFormat:@"%i,%i", lat_int, lng_int]];
    if (cityDic) {
        return cityDic;
    }
#if 1
    NSInteger lat_floor = floor(coordinate.latitude);
    NSInteger lng_floor = floor(coordinate.longitude);
    NSString *lat_lng = [NSString stringWithFormat:@"%i,%i", lat_floor, lng_floor];
    cityIDArray = [GetAppDelegate().locationTocityIdDic objectForKey:lat_lng];
#else
    NSString *whereClause = [NSString stringWithFormat:@"WHERE %@ = %.0f AND %@ = %.0f", @"lat", floor(coordinate.latitude), @"lng", floor(coordinate.longitude)];
    NSString *sqlCoord2CityId = [NSString stringWithFormat:@"SELECT * FROM %@ %@", DBCoord2CityidTable, whereClause];
    FMResultSet *rs = [db executeQuery:sqlCoord2CityId];
    while ([rs next]) {
        NSDictionary *polygonCityArea = [NSDictionary dictionaryWithObjectsAndKeys:[rs objectForColumnName:@"cityId"], @"cityId", [rs objectForColumnName:DBCityAreaNo], @"no", nil];
        [cityIDArray addObject:polygonCityArea];
    }
    [rs close];
#endif
    //判断照片坐标点是否在城市内 过滤cityid
    if (cityIDArray.count > 1) {
        //NSLog(@"cityIDArray count is %i coordinate (%f, %f)", cityIDArray.count, coordinate.latitude, coordinate.longitude);
        BOOL Have = NO;
        for (NSDictionary *polygonCityArea in cityIDArray) {
            @autoreleasepool {
                NSInteger cityid = [[polygonCityArea objectForKey:@"cityId"] integerValue];
                NSInteger no = [[polygonCityArea objectForKey:@"no"] integerValue];
                
                NSString *whereClause = [NSString stringWithFormat:@"WHERE %@ = %i AND %@ = %i", @"cityId", cityid , @"no", no];
                NSString *sql = [NSString stringWithFormat:@"SELECT * FROM %@ %@", DBPointInPolygonTable, whereClause];
                FMResultSet *rs = [db executeQuery:sql];
                NSMutableArray *latArray = [NSMutableArray array];
                NSMutableArray *lngArray = [NSMutableArray array];
                while ([rs next]) {
                    [latArray addObject:[rs objectForColumnName:@"lat"]];
                    [lngArray addObject:[rs objectForColumnName:@"lng"]];
                }
                [rs close];
                
                BOOL isIn = isPointInPolygon(coordinate, latArray, lngArray);
                if (isIn) {
                    Have = YES;
                    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:cityid], DBCityID, nil];
                    dic.associatedObject = [NSNumber numberWithInteger:latArray.count];
                    [filterIDArray addObject:dic];
                }
            }
        }
        if (!Have) {
            NSLog(@"wtf %f %f ", coordinate.latitude, coordinate.longitude);
        }
    } else if (1 == cityIDArray.count) {
        //此处不严谨
        NSNumber *cityId = [[cityIDArray objectAtIndex:0] objectForKey:@"cityId"];
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:cityId, DBCityID,nil];
        [filterIDArray addObject:dic];
    } else {
        //NSLog(@"no message coordinate is %f %f", coordinate.latitude, coordinate.longitude);
    }
    
    //NSLog(@"TAQuery filter city %f", [[NSDate date] timeIntervalSince1970]);
    if (filterIDArray.count > 0) {
        NSDictionary *cityDic= GetSmallCityDic(filterIDArray);
        NSNumber *cityId = [cityDic objectForKey:DBCityID];
        NSString *cityName = [GetAppDelegate().cityid2NameMapDic objectForKey:cityId];
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:cityName, DBCityName, cityId, DBCityID, nil];
        
        NSInteger lat_int = (coordinate.latitude * 1000);
        NSInteger lng_int = (coordinate.longitude * 1000);
        for (int i = -1; i < 1; i++) {
            for (int j = -1; j < 1; j++) {
                @autoreleasepool {
                    NSInteger lat = lat_int + i;
                    NSInteger lng = lng_int + j;
                    NSMutableDictionary *subDic = [GetAppDelegate().nearbyLocationToCityMap objectForKey:[NSString stringWithFormat:@"%i,%i", lat, lng]];
                    if (!subDic) {
                        [GetAppDelegate().nearbyLocationToCityMap setObject:dic forKey:[NSString stringWithFormat:@"%i,%i", lat, lng]];
                    }
                }
            }
        }
        return dic;
    }
    
    [filterIDArray removeAllObjects];
    [cityIDArray removeAllObjects];
    
    NSDictionary *noFoundDic = [NSDictionary dictionaryWithObjectsAndKeys:DBUncategorizedCityName, DBCityName, [NSNumber numberWithInteger:DBUncategorizedCityID], DBCityID, nil];
    for (int i = -1; i < 1; i++) {
        for (int j = -1; j < 1; j++) {
            @autoreleasepool {
                NSInteger lat = lat_int + i;
                NSInteger lng = lng_int + j;
                NSMutableDictionary *subDic = [GetAppDelegate().nearbyLocationToCityMap objectForKey:[NSString stringWithFormat:@"%i,%i", lat, lng]];
                if (!subDic) {
                    [GetAppDelegate().nearbyLocationToCityMap setObject:noFoundDic forKey:[NSString stringWithFormat:@"%i,%i", lat, lng]];
                }
            }
        }
    }
    //NSLog(@"TAQuery end %f", [[NSDate date] timeIntervalSince1970]);
    return noFoundDic;
}


void TAEndEnumeratePhotosWithWillChangedPhotos(NSMutableArray *savePhotosArray, NSMutableArray *deletePhotosArray) {
        
    dispatch_async([TATool saveToUserDBQueue], ^{
        FMDatabase *userDB = [TATool sharedUserDB];
        [userDB beginTransaction];
        for (NSDictionary *dictionary in savePhotosArray) {
            DBWriteToUser(userDB, DBCity2AssetURLTable, dictionary, NULL);
        }
        [userDB commit];
        if (deletePhotosArray.count > 0) {
            [userDB beginTransaction];
            for (NSString *assetURL in deletePhotosArray) {
                
                NSLog(@"delete one url is %@",assetURL);
                NSDictionary *column = [NSDictionary dictionaryWithObjectsAndKeys:assetURL, DBPhotoAssetURL, nil];
                DBDeleteFromUser(userDB, DBCity2AssetURLTable, column, NULL);
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDeletePhotoCity object:column userInfo:nil];
            }
            [userDB commit];
        }
        NSLog(@"userDB end %f",[[NSDate date] timeIntervalSince1970]);
        [userDB close];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRefreshUIFromDB object:nil userInfo:nil];
        [GetAppDelegate().cityPolygonDic removeAllObjects];
        [GetAppDelegate().cityid2NameMapDic removeAllObjects];
        [GetAppDelegate().nearbyLocationToCityMap removeAllObjects];
        if (GetAppDelegate().isEnterForeground) {
            //EnumerateAllPhotosToDB();
            GetAppDelegate().isEnterForeground = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEnumerateAllPhotos object:nil userInfo:nil];
        }
    });
}
NSDictionary *GetSmallCityDic(NSMutableArray *unSortArray) {
    int count = unSortArray.count;
    if (count == 1) {
        return [unSortArray objectAtIndex:0];
    } else {
        //NSLog(@"cityCount is %i", count);
        for (NSDictionary *dic in unSortArray) {
            //NSLog(@"cityId is %@", [dic objectForKey:DBCityID]);
        }
    }
    int numOfPoints = 0;
    int sort = 0;
    for (int i = 0; i < count; i++) {
        NSDictionary *item = [unSortArray objectAtIndex:i];
        int num = [item.associatedObject integerValue];
        if (0 == i) {
            numOfPoints = num;
            sort = 0;
        } else {
            if (num < numOfPoints ) {
                numOfPoints = num;
                sort = i;
            }
        }
    }
    return [unSortArray objectAtIndex:sort];
}
void CreateUserDB(void) {

    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isExist = [fileManager fileExistsAtPath:[TATool shareUserDBFilePath]];
    if (isExist) {
        NSLog(@"UserD exist");
        //[fileManager removeItemAtPath:[TATool shareUserDBFilePath] error:nil];
        return;
    }
    FMDatabase *db = [FMDatabase databaseWithPath:[TATool shareUserDBFilePath]];
    if (![db open]) {
        NSLog(@"why db cannot open!");
        return;
    }
    NSArray *allTables = @[DBCity2AssetURLTable];
    for (NSString *table in allTables) {
        NSArray *schema = GetTableSchema(table);
        CreateTableSchema(db, table, schema);
    }
    
    NSString *indexName = DBGetIndexName(DBCity2AssetURLTable, DBCityID);
    DBCreateIndex(db, DBCity2AssetURLTable, @[[@"\"cityID\"" stringByAppendingString:@" ASC"], [@"\"assetCreateDate\"" stringByAppendingString:@" DESC"]], indexName);
    [db close];
}

NSString* DBGetIndexName(NSString *table, NSString *column)
{
    return [@"pxindex" stringByAppendingFormat:@"_%@_%@", table, column];;
}


NSArray* GetTableSchema(NSString *table)
{
    
    if ([table isEqualToString:DBCity2AssetURLTable]) {
        return @[
        @{DBSchemaKeyColumnName : DBCityID, DBSchemaKeyColumnDatatype : PXDBDatatypeINTEGER},
        @{DBSchemaKeyColumnName : DBphotoCreateDate, DBSchemaKeyColumnDatatype : PXDBDatatypeINTEGER},
        @{DBSchemaKeyColumnName : DBCityName, DBSchemaKeyColumnDatatype : PXDBDatatypeTEXT},
        @{DBSchemaKeyColumnName : DBPhotoAssetURL, DBSchemaKeyColumnDatatype : PXDBDatatypeTEXT},
        //@{DBSchemaKeyColumnName : DBPhotoAsset, DBSchemaKeyColumnDatatype : PXDBDatatypeBLOB},
        ];
    } else if ([table isEqualToString:DBAssetURLsTable]) {
        return @[
        @{DBSchemaKeyColumnName : DBCityID, DBSchemaKeyColumnDatatype : PXDBDatatypeINTEGER, DBSchemaKeyColumnConstraint : PXDBConstraintPRIMARYKEY},
        @{DBSchemaKeyColumnName : DBCityName, DBSchemaKeyColumnDatatype : PXDBDatatypeTEXT},
        @{DBSchemaKeyColumnName : DBphotoAssetURLs, DBSchemaKeyColumnDatatype : PXDBDatatypeBLOB},
        ];
    }
    return nil;
}

void CreateTableSchema(FMDatabase *db, NSString *table, NSArray *schema)
{
    if (![db tableExists:table]) {
        //根据schema 创建表结构
        NSMutableArray *parameters = [NSMutableArray arrayWithCapacity:schema.count];
        for (NSDictionary *dic in schema) {
            NSString *columnName = dic[DBSchemaKeyColumnName];
            NSString *columnDatatype = dic[DBSchemaKeyColumnDatatype];
            NSString *columnConstraint = dic[DBSchemaKeyColumnConstraint];
            if (!columnConstraint) {
                columnConstraint = @"";
            }
            NSString *parameter = [columnName stringByAppendingFormat:@" %@ %@", columnDatatype, columnConstraint];
            [parameters addObject:parameter];
        }
        
        NSString *columns = [parameters componentsJoinedByString:@","];
        NSString *sql = [NSString stringWithFormat:@"CREATE TABLE %@ (%@)", table, columns];
        [db executeUpdate:sql];
    }
    //检查每个字段
    for (NSDictionary *dic in schema) {
        NSString *columnName = [dic objectForKey:DBSchemaKeyColumnName];
        if (![db columnExists:columnName inTableWithName:table]) {
            NSString *columnDatatype = dic[DBSchemaKeyColumnDatatype];
            NSString *columnConstraint = dic[DBSchemaKeyColumnConstraint];
            if (!columnConstraint) {
                columnConstraint = @"";
            }
            NSString *sql = [NSString stringWithFormat:@"ALTER TABLE %@ ADD %@ %@ %@", table, columnName, columnDatatype, columnConstraint];
            [db executeUpdate:sql];
        }
    }
}

void DBCreateIndex(FMDatabase *db, NSString *table, NSArray *columnNames, NSString *indexName)
{
    NSLog(@"on table %@ columnNames %@ indexName %@", table, columnNames, indexName);
    NSString *columns = [columnNames componentsJoinedByString:PXDBSeparator];
    NSString *sql = [NSString stringWithFormat:@"CREATE INDEX IF NOT EXISTS \"%@\" ON \"%@\" (%@)", indexName, table, columns];
    NSLog(@"sql %@", sql);
    [db executeUpdate:sql];
}


void DBDeleteFromUser(FMDatabase *db, NSString *table, NSDictionary *columns, DDBlockVoid completion) {
    if (columns && columns.count > 0) {
        NSString *whereClause = DBMakeWhereClause(columns);
        NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ %@",table,whereClause];
        NSLog(@"delete sql is %@", sql);
        [db executeUpdate:sql];
    } else {
        NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@",table];
        [db executeUpdate:sql];
    }
    if (completion) {
        completion();
    }
}
void DBWriteToUser(FMDatabase *db, NSString *table, NSDictionary *columns, DDBlockVoid completion) {
    NSString *whereClause = DBMakeWhereClause(columns);
    //NSLog(@"whereClause is %@", whereClause);
    BOOL isExist = DBCheckQueryExist(db, table, whereClause);
    //NSLog(@"isExist is  %i", isExist);
    if (!isExist) {
        NSMutableArray *parameters = [NSMutableArray arrayWithCapacity:columns.count];
        NSMutableArray *arguments = [NSMutableArray arrayWithCapacity:columns.count];
        NSMutableArray *placeholders = [NSMutableArray arrayWithCapacity:columns.count];
        
        for (NSString *columnName in columns) {
            id columnValue = [columns objectForKey:columnName];
            NSString *parameter = [NSString stringWithFormat:@"%@", columnName];
            [parameters addObject:parameter];
            [placeholders addObject:PXDBPlaceholder];
            [arguments addObject:columnValue];
        }
       
        NSString *sql = [NSString stringWithFormat:@"INSERT INTO %@ (%@) VALUES (%@)", table, [parameters componentsJoinedByString:PXDBSeparator], [placeholders componentsJoinedByString:PXDBSeparator]];
        //NSLog(@"execute sql %@", sql);
        //static NSInteger count = 0;
        //NSLog(@"photos have %i", ++count);
        [db executeUpdate:sql withArgumentsInArray:arguments];
    }
    if (completion) {
        completion();
    }
}

BOOL DBReadFromTable(NSString *table, NSDictionary *keyColumns, NSMutableDictionary *columns)
{
    NSLog(@"table %@ keyColumns %@ columns %@", table, keyColumns, columns);
    
    __block BOOL rowExists = NO;
    FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath:[TATool shareUserDBFilePath]];
    [queue inDatabase:^(FMDatabase *db) {
        NSString *sql = nil;
        if (!keyColumns) {
            sql = [NSString stringWithFormat:@"SELECT * FROM %@", table];
        }else {
            NSString *whereClause = DBMakeWhereClause(keyColumns);
            sql = [NSString stringWithFormat:@"SELECT * FROM %@ %@", table, whereClause];
        }
        
        FMResultSet *resultSet = [db executeQuery:sql];
        NSLog(@"execute sql %@", sql);
        NSMutableArray *resultArray = [NSMutableArray array];
        NSMutableArray *photoURLArray = [NSMutableArray array];
        NSInteger cityID = -999;
        NSString *tempCityName = nil;
        if ([resultSet next]) {
            rowExists = YES;
            
            NSInteger tempID = [[resultSet objectForColumnName:DBCityID] integerValue];
            NSString *photoURL = [resultSet objectForColumnName:DBPhotoAssetURL];
            if (-999 == cityID) {
                tempCityName = [resultSet objectForColumnName:DBCityName];
                cityID = tempID;
                [photoURLArray addObject:photoURL];
                
            } else if (tempID == cityID) {
                tempCityName = [resultSet objectForColumnName:DBCityName];
                [photoURLArray addObject:photoURL];
            } else if (tempID != cityID) {
                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:tempCityName, DBCityName, photoURLArray, DBphotoAssetURLs, nil];
                [resultArray addObject:dic];
                [photoURLArray removeAllObjects];
                tempCityName = [resultSet objectForColumnName:DBCityName];
                [photoURLArray addObject:photoURL];
            }
        }
        if (tempCityName) {
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:tempCityName, DBCityName, photoURLArray, DBphotoAssetURLs, nil];
            [resultArray addObject:dic];
            [photoURLArray removeAllObjects];
        }
        if (columns) {
            [columns setObject:resultArray forKey:@"cityGroup"];
        }
        [resultSet close];
    }];
    [queue close];
    return rowExists;
}

NSString* DBMakeWhereClause(NSDictionary *keyColumns)
{
    NSString *whereClause = nil;
    for (NSString *keyColumnName in keyColumns) {
        //试图用 keyColumn 作为关键字进行 SELECT 操作
        id keyColumnValue = [keyColumns objectForKey:keyColumnName];
        if (!whereClause) {
            whereClause = [NSString stringWithFormat:@"WHERE %@ = '%@'", keyColumnName, keyColumnValue];
        } else {
            whereClause = [whereClause stringByAppendingFormat:@" AND %@ = '%@'", keyColumnName, keyColumnValue];
        }
    }
    return whereClause;
}

BOOL DBCheckQueryExist(FMDatabase *db, NSString *table, NSString *whereClause) {
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM %@ %@", table, whereClause];
   // NSLog(@"select sql %@", sql);
    FMResultSet *resultSet = [db executeQuery:sql];
    if ([resultSet next]) {
        [resultSet close];
        return YES;
    }
    return NO;
}

NSMutableDictionary *DBReadCityPolygon(FMDatabase *db) {
    NSMutableDictionary *theCityPolygonArray = [NSMutableDictionary dictionary];
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM %@", DBPointInPolygonTable];
    FMResultSet *rs = [db executeQuery:sql];
    NSMutableArray *latArray = [NSMutableArray array];
    NSMutableArray *lngArray = [NSMutableArray array];
    NSMutableArray *polygonArray = [NSMutableArray array];
    NSInteger areaNo = 0;
    NSInteger cityId = -999;
    while ([rs next]) {
        //NSLog(@" cityid is %i", cityId);
        if ( -999 == cityId) {
            cityId = [[rs objectForColumnName:@"cityId"] integerValue];
            if (areaNo == [[rs objectForColumnName:@"no"] integerValue]) {
                NSLog(@"first cityid is %i", cityId );
                [latArray addObject:[rs objectForColumnName:@"lat"]];
                [lngArray addObject:[rs objectForColumnName:@"lng"]];
            }
        } else if (cityId == [[rs objectForColumnName:@"cityId"] integerValue]) {
            //NSLog(@" areaNo is %i", areaNo);
            if (areaNo == [[rs objectForColumnName:@"no"] integerValue]) {
                [latArray addObject:[rs objectForColumnName:@"lat"]];
                [lngArray addObject:[rs objectForColumnName:@"lng"]];
            } else {
                areaNo = [[rs objectForColumnName:@"no"] integerValue];
                [polygonArray addObject:[NSMutableArray arrayWithArray:latArray]];
                [polygonArray addObject:[NSMutableArray arrayWithArray:lngArray]];
                [latArray removeAllObjects];
                [lngArray removeAllObjects];
                [latArray addObject:[rs objectForColumnName:@"lat"]];
                [lngArray addObject:[rs objectForColumnName:@"lng"]];
            }
        } else if (cityId != [[rs objectForColumnName:@"cityId"] integerValue]) {
            [polygonArray addObject:[NSMutableArray arrayWithArray:latArray]];
            [polygonArray addObject:[NSMutableArray arrayWithArray:lngArray]];
            [theCityPolygonArray setObject:[NSMutableArray arrayWithArray:polygonArray] forKey:[NSNumber numberWithInteger:cityId]];
            //NSLog(@"cityPolygonDic is %@", GetAppDelegate().cityPolygonDic);
            cityId = [[rs objectForColumnName:@"cityId"] integerValue];
            areaNo = 0;
            [latArray removeAllObjects];
            [lngArray removeAllObjects];
            [polygonArray removeAllObjects];
            [latArray addObject:[rs objectForColumnName:@"lat"]];
            [lngArray addObject:[rs objectForColumnName:@"lng"]];
        }
    }
    
    [polygonArray addObject:[NSMutableArray arrayWithArray:latArray]];
    [polygonArray addObject:[NSMutableArray arrayWithArray:lngArray]];
    [theCityPolygonArray setObject:[NSMutableArray arrayWithArray:polygonArray] forKey:[NSNumber numberWithInteger:cityId]];
    [latArray removeAllObjects];
    [lngArray removeAllObjects];
    [polygonArray removeAllObjects];
    [rs close];
    return theCityPolygonArray;
}

NSMutableDictionary *DBReadCityNameByID(FMDatabase *db) {
    NSMutableDictionary *theCityNameByID = [NSMutableDictionary dictionary];
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM %@", DBCityid2CityNameTable];
    FMResultSet *rs = [db executeQuery:sql];
    while ([rs next]) {
        NSString *cityName = [rs objectForColumnName:@"name"];
        NSNumber *cityID = [rs objectForColumnName:@"id"];
        [theCityNameByID setObject:[NSString stringWithString:cityName] forKey:cityID];
    }
    [rs close];
    return theCityNameByID;
}

NSMutableDictionary *DBReadCityIDByLocation(FMDatabase *db) {
    NSMutableDictionary *theCityIDsDic = [NSMutableDictionary dictionary];
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM %@", DBCoord2CityidTable];
    FMResultSet *rs = [db executeQuery:sql];
    while ([rs next]) {
        @autoreleasepool {
            CGFloat subLat = [[rs objectForColumnName:@"lat"] floatValue];
            CGFloat subLng = [[rs objectForColumnName:@"lng"] floatValue];
            NSInteger lat_int = subLat;
            NSInteger lng_int = subLng;
            NSNumber *cityId = [rs objectForColumnName:@"cityId"];
            NSNumber *no = [rs objectForColumnName:DBCityAreaNo];
            NSString *lat_lng = [NSString stringWithFormat:@"%i,%i", lat_int, lng_int];
             NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:cityId, @"cityId", no, @"no", nil];
            if ([theCityIDsDic objectForKey:lat_lng]) {
                //NSLog(@"add dic to array");
                NSMutableArray *array = [theCityIDsDic objectForKey:lat_lng];
                [array addObject:dic];
            } else {
                //NSLog(@"init array");
                NSMutableArray *array = [NSMutableArray array];
                [array addObject:dic];
                [theCityIDsDic setObject:array forKey:lat_lng];
            }
        }
    }
    return theCityIDsDic;
}

void TAProcessUncategorizedPhotos(void) {
    dispatch_async([TATool saveToUserDBQueue], ^{
         NSLog(@"read uncategorized photos");
        [GetAppDelegate().uncategorizedArray removeAllObjects];
        FMDatabase *userDB = [TATool sharedUserDB];
        [userDB open];
        NSString *whereClause = [NSString stringWithFormat:@"WHERE %@ = %i", DBCityID, DBUncategorizedCityID];
        NSString *sql = [NSString stringWithFormat:@"SELECT * FROM %@ %@", DBCity2AssetURLTable, whereClause];
        FMResultSet *rs = [userDB executeQuery:sql];
        while ([rs next]) {
            NSURL *assetURL = [NSURL URLWithString:[rs objectForColumnName:DBPhotoAssetURL]];
            [GetAppDelegate().uncategorizedArray addObject:assetURL];
        }
        [rs close];
        [userDB close];
        //使用CLGeocoder 得到城市名称
        TARequestPhotoCityName();
    });

}

void TARequestPhotoCityName(void) {
    dispatch_async(TAGetGlobalQueue(), ^{
        //如果用户再次进入应用，停止当前工作
        if (GetAppDelegate().isEnumerateAllPhotos) {
            [GetAppDelegate().uncategorizedArray removeAllObjects];
            return;
        }
        //已经没有未分类的照片，停止并发出通知，准备刷新页面
        if (0 == GetAppDelegate().uncategorizedArray.count) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRefreshUIFromDB object:[NSNumber numberWithBool:YES] userInfo:nil];
            return;
        }
        NSURL *assetURL = [GetAppDelegate().uncategorizedArray objectAtIndex:0];
        [[TATool assetsLibrary] assetForURL:assetURL resultBlock:^(ALAsset *asset) {
            if (!asset) {
                TARequestPhotoCityName();
                return ;
            }
            //获取照片地理位置坐标
            CLLocationCoordinate2D coordinate;
            @autoreleasepool {
                NSDictionary *metadata = asset.defaultRepresentation.metadata;
                if (metadata && [metadata valueForKey:@"{GPS}"]) {
                    coordinate.latitude = [[metadata valueForKeyPath:@"{GPS}.Latitude"] doubleValue];
                    coordinate.longitude = [[metadata valueForKeyPath:@"{GPS}.Longitude"] doubleValue];
                    NSString *latitudeRef = [metadata valueForKeyPath:@"{GPS}.LatitudeRef"];
                    if ([latitudeRef isEqualToString:@"S"]) {
                        coordinate.latitude = -coordinate.latitude;
                    }
                    NSString *longitudeRef = [metadata valueForKeyPath:@"{GPS}.LongitudeRef"];
                    if ([longitudeRef isEqualToString:@"W"]) {
                        coordinate.longitude = -coordinate.longitude;
                    }
                }
            }
            if (CLLocationCoordinate2DIsValid(coordinate)) {
                
                NSNumber *photoDate = [NSNumber numberWithInteger:[[asset valueForProperty:ALAssetPropertyDate] timeIntervalSince1970]];
                NSInteger lat_int = (coordinate.latitude * 1000);
                NSInteger lng_int = (coordinate.longitude * 1000);
                NSMutableDictionary *cityDic = [GetAppDelegate().nearbyLocationToCityMap objectForKey:[NSString stringWithFormat:@"%i,%i", lat_int, lng_int]];
                if (cityDic) {
                    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:DBGoogleCityID], DBCityID,[cityDic objectForKey:DBCityName], DBCityName, assetURL.absoluteString, DBPhotoAssetURL, photoDate, DBphotoCreateDate, nil];
                    TAReCategorizePhoto(dic);
                    [GetAppDelegate().uncategorizedArray removeObjectAtIndex:0];
                    TARequestPhotoCityName();
                    return ;
                }
#if 0
                CLLocation *location = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
                CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
                [geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
                    CLPlacemark *placemark = [placemarks objectAtIndex:0];
                    dispatch_async(TAGetGlobalQueue(), ^{
                        NSString *cityName = [placemark locality]? [placemark locality]: [placemark administrativeArea];
                        NSDictionary *dic = nil;
                        if (cityName) {
                            if (GetAppDelegate().isEnumerateAllPhotos) {
                                return;
                            }
                            NSLog(@"city name is %@ and %@", [placemark locality], [placemark administrativeArea]);
                            dic = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:DBGoogleCityID], DBCityID, cityName, DBCityName, assetURL.absoluteString, DBPhotoAssetURL, photoDate, DBphotoCreateDate, nil];
                            //将修改用户数据库信息，删除未分类的信息
                            TAReCategorizePhoto(dic);
                        } else {
                            NSLog(@"placemark is %@ location is %f,%f error is %@", placemark, location.coordinate.latitude, location.coordinate.longitude, error.description);
                        }
                        [GetAppDelegate().uncategorizedArray removeObjectAtIndex:0];
                        
                        if (dic) {
                            for (int i = -1; i < 1; i++) {
                                for (int j = -1; j < 1; j++) {
                                    @autoreleasepool {
                                        NSInteger lat = lat_int + i;
                                        NSInteger lng = lng_int + j;
                                        NSMutableDictionary *subDic = [GetAppDelegate().nearbyLocationToCityMap objectForKey:[NSString stringWithFormat:@"%i,%i", lat, lng]];
                                        if (!subDic) {
                                            [GetAppDelegate().nearbyLocationToCityMap setObject:dic forKey:[NSString stringWithFormat:@"%i,%i", lat, lng]];
                                        } else {
                                        }
                                    }
                                }
                            }
                        }
                        NSLog(@"next");
                        TARequestPhotoCityName();
                        
                    });   
                }];
#else
                PUGetCityNameByCoord(coordinate, kPlacemarkCountryCN, ^(id obj) {
                    dispatch_async(TAGetGlobalQueue(), ^{
                        NSLog(@"cityName is %@", obj);
                        NSDictionary *dic = nil;
                        if (obj) {
                            NSString *cityName = obj;
                            if (GetAppDelegate().isEnumerateAllPhotos) {
                                return;
                            }
                            dic = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:DBGoogleCityID], DBCityID, cityName, DBCityName, assetURL.absoluteString, DBPhotoAssetURL, photoDate, DBphotoCreateDate, nil];
                            TAReCategorizePhoto(dic);
                        }
                        [GetAppDelegate().uncategorizedArray removeObjectAtIndex:0];
                        
                        if (dic) {
                            for (int i = -1; i < 1; i++) {
                                for (int j = -1; j < 1; j++) {
                                    @autoreleasepool {
                                        NSInteger lat = lat_int + i;
                                        NSInteger lng = lng_int + j;
                                        NSMutableDictionary *subDic = [GetAppDelegate().nearbyLocationToCityMap objectForKey:[NSString stringWithFormat:@"%i,%i", lat, lng]];
                                        if (!subDic) {
                                            [GetAppDelegate().nearbyLocationToCityMap setObject:dic forKey:[NSString stringWithFormat:@"%i,%i", lat, lng]];
                                        } else {
                                        }
                                    }
                                }
                                
                            }
                        }
                        TARequestPhotoCityName();
                        NSLog(@"next");
                    });
                });
#endif
            }
        } failureBlock:^(NSError *error) {
            NSLog(@"read photo failed");
        }];
    });
}

void TAReCategorizePhoto(NSDictionary *columns) {
    FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath:[TATool shareUserDBFilePath]];
    [queue inDatabase:^(FMDatabase *db) {
        NSString *whereClause = DBMakeWhereClause(columns);
        //查看数据是否存在
        BOOL isExist = DBCheckQueryExist(db, DBCity2AssetURLTable, whereClause);
        if (!isExist) {
            //不存在，删除原有的，添加新的，并发出通知
            NSDictionary *columnsUncategorize = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:DBUncategorizedCityID], DBCityID, [columns objectForKey:DBPhotoAssetURL], DBPhotoAssetURL, nil];
            NSString *whereClauseUncategorize = DBMakeWhereClause(columnsUncategorize);
            BOOL isExistUncategorize = DBCheckQueryExist(db, DBCity2AssetURLTable, whereClauseUncategorize);
            if (isExistUncategorize) {
                NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ %@", DBCity2AssetURLTable, whereClauseUncategorize];
               // NSLog(@"delete uncategorized sql %@", sql);
                [db executeUpdate:sql];
#if 0
                //暂时注释掉，页面暂时不显示未分类
                NSMutableDictionary *notifiDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:DBUncategorizedCityName, DBCityName, nil];
                [notifiDic addEntriesFromDictionary:columnsUncategorize];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDeletePhotoCity object:notifiDic userInfo:nil];
#endif
            }
            NSMutableDictionary *saveDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:-2], DBCityID,nil];
            [saveDic addEntriesFromDictionary:columns];
           // NSLog(@"add photo to db");
            NSMutableArray *parameters = [NSMutableArray arrayWithCapacity:saveDic.count];
            NSMutableArray *arguments = [NSMutableArray arrayWithCapacity:saveDic.count];
            NSMutableArray *placeholders = [NSMutableArray arrayWithCapacity:saveDic.count];
            
            for (NSString *columnName in saveDic) {
                id columnValue = [saveDic objectForKey:columnName];
                NSString *parameter = [NSString stringWithFormat:@"%@", columnName];
                [parameters addObject:parameter];
                [placeholders addObject:PXDBPlaceholder];
                [arguments addObject:columnValue];
            }
            NSString *sql = [NSString stringWithFormat:@"INSERT INTO %@ (%@) VALUES (%@)", DBCity2AssetURLTable, [parameters componentsJoinedByString:PXDBSeparator], [placeholders componentsJoinedByString:PXDBSeparator]];
            [db executeUpdate:sql withArgumentsInArray:arguments];
            [[NSNotificationCenter defaultCenter] postNotificationName: kNotificationGetCityName object:columns userInfo:nil];
        } else {
            NSDictionary *columnsUncategorize = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithInteger:DBUncategorizedCityID], DBCityID, [columns objectForKey:DBPhotoAssetURL], DBPhotoAssetURL, nil];
            NSString *whereClauseUncategorize = DBMakeWhereClause(columnsUncategorize);
            BOOL isExistUncategorize = DBCheckQueryExist(db, DBCity2AssetURLTable, whereClauseUncategorize);
            if (isExistUncategorize) {
                NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ %@", DBCity2AssetURLTable, whereClauseUncategorize];
                //NSLog(@"delete uncategorized sql %@", sql);
                [db executeUpdate:sql];
                NSMutableDictionary *notifiDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:DBUncategorizedCityName,DBCityName, nil];
                [notifiDic addEntriesFromDictionary:columnsUncategorize];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDeletePhotoCity object:notifiDic userInfo:nil];
            }
        }
    }];
    [queue close];
}
#pragma mark - archive / unarchive

NSData* PUArchiveObject(id obj)
{
    if (!obj) {
        return nil;
    }
    NSData *data = nil;
    @try {
        data = [NSKeyedArchiver archivedDataWithRootObject:obj];
    }
    @catch (NSException *exception) {
        NSLog(@"PUArchiveObject exception %@", exception);
    }
    @finally {
    }
    return data;
}

id PUUnarchiveData(NSData *data)
{
    if (!data) {
        return nil;
    }
    id obj = nil;
    @try {
        obj = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    @catch (NSException *exception) {
        NSLog(@"PUUnarchiveData exception %@", exception);
    }
    @finally {
    }
    return obj;
}

#pragma mark - zip/unzip
void UnzipDBFile(void) {
    NSLog(@"unzip start at %f", [[NSDate date] timeIntervalSince1970]);
    NSString *filepath = [[NSBundle mainBundle] pathForResource:@"TravelAlbum" ofType:@"zip"];
     NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *path = [docsPath stringByAppendingPathComponent:@"TravelAlbum.db"];

    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL success = [fileManager fileExistsAtPath:path];
    if (success) {
        NSLog(@"db exist");
        return;
    }
    ZipArchive *zipArchive = [[ZipArchive alloc] init];
    
    if ([zipArchive UnzipOpenFile:filepath]) {
        NSLog(@"can unzip source file");
        if ([zipArchive UnzipFileTo:docsPath overWrite:YES]) {
            NSLog(@"success");
        }
    }
    [zipArchive UnzipCloseFile];
    NSLog(@"unzip end at %f", [[NSDate date] timeIntervalSince1970]);
}

#pragma mark - Queue
dispatch_queue_t TAGetMainQueue(void)
{
    return dispatch_get_main_queue();
}

dispatch_queue_t TAGetGlobalQueue(void)
{
    return dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
}

void CheckQueue(dispatch_queue_t queue)
{
    if (queue == TAGetMainQueue()) {
        NSLog(@"CheckQueue is main");
    }else if (queue == TAGetGlobalQueue()) {
        NSLog(@"CheckQueue is global");
    }else {
        NSLog(@"CheckQueue is serial");
    }
}

void CheckCurrentQueue(void)
{
    CheckQueue(dispatch_get_current_queue());
}

BOOL isPointIn(void) {
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(39.287167, -76.607167);
//    float lat[] = {18.589322,18.558449,18.482545,18.452264,18.418876,18.303868,18.29284,18.261008,18.257069,18.178738,18.125832,18.122526,18.149097,18.280688,18.416036,18.517261,18.447797,18.393442,18.432482,18.536163,18.575033};
//    float lng[] = {109.356954,109.265271,109.188018,109.097331,109.012377,108.938477,109.090798,109.186848,109.288683,109.414696,109.506554,109.618107,109.725587,109.805445,109.796715,109.787697,109.708753,109.609669,109.522624,109.54296,109.4432};
    float lat[] = {39.23259,
        39.27455,
        39.37193,
        39.37194,
        39.29689,
        39.20104};
    float lng[] = {-76.60961,
        -76.7032,
        -76.71131,
        -76.59018,
        -76.52952,
        -76.56257};
    NSMutableArray *latArray = [NSMutableArray array];
    NSMutableArray *lngArray = [NSMutableArray array];
    for (int i = 0; i< 6; i++) {
        [latArray addObject:[NSNumber numberWithFloat:lat[i]]];
        [lngArray addObject:[NSNumber numberWithFloat:lng[i]]];
    }
    NSLog(@"loop start %f", [[NSDate date] timeIntervalSince1970]);
    for (int z = 0; z <  13 * 100; z++) {
        CGFloat latMin = [[latArray valueForKeyPath:@"@min.self"] floatValue];
        CGFloat latMax = [[latArray valueForKeyPath:@"@max.self"] floatValue];
        CGFloat lngMin = [[lngArray valueForKeyPath:@"@min.self"] floatValue];
        CGFloat lngMax = [[lngArray valueForKeyPath:@"@max.self"] floatValue];
        
        if (coordinate.latitude < latMin || coordinate.latitude > latMax || coordinate.longitude < lngMin || coordinate.longitude > lngMax) {
            // We're outside the polygon!
            NSLog(@"outside");
            break;
        }
        
        int Num = latArray.count;
        
        //copy code
#if 1
        int i, j, c = 0;
        for (i = 0, j = Num - 1; i < Num; j = i++) {
            CGFloat latI = [[latArray objectAtIndex:i] floatValue];
            CGFloat latJ = [[latArray objectAtIndex:j] floatValue];
            CGFloat lngI = [[lngArray objectAtIndex:i] floatValue];
            CGFloat lngJ = [[lngArray objectAtIndex:j] floatValue];
            if ( ((lngI >coordinate.longitude) != (lngJ>coordinate.longitude)) &&
                (coordinate.latitude < (latJ-latI) * (coordinate.longitude-lngI) / (lngJ-lngI) + latI)) {
                c = !c;
            }
        }
        NSLog(@"loop end c %i", c);
        return c;
    }
#else
        int i, j = Num - 1;
        bool c = NO;
        
        for (i= 0; i < Num; i++) {
            CGFloat latI = [[latArray objectAtIndex:i] floatValue];
            CGFloat latJ = [[latArray objectAtIndex:j] floatValue];
            CGFloat lngI = [[lngArray objectAtIndex:i] floatValue];
            CGFloat lngJ = [[lngArray objectAtIndex:j] floatValue];
            if ((lngI < coordinate.longitude && lngJ >= coordinate.longitude)
                 ||  (lngJ < coordinate.longitude && lngI >= coordinate.longitude)) {
                if ((latI +(coordinate.longitude - lngI) / (lngJ-lngI) * (latJ-lngI)) < coordinate.latitude) {
                    c = !c;
                }
            }
            j=i;
        }
        NSLog(@"loop end c %i", c);
        return c;
    }
#endif
    NSLog(@"loop end %f", [[NSDate date] timeIntervalSince1970]);
    return YES;
}

BOOL isPointInPolygon(CLLocationCoordinate2D coordinate, NSMutableArray *latArray, NSMutableArray *lngArray) {
    if (0 == latArray.count) {
        return NO;
    }
    if (coordinate.latitude == 39.287167 && coordinate.longitude == -76.607167) {
        NSLog(@"bingo");
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:latArray, @"lat", lngArray, @"lng", nil];
        NSLog(@"%@", dic);
    }
    CGFloat latMin = [[latArray valueForKeyPath:@"@min.self"] floatValue];
    CGFloat latMax = [[latArray valueForKeyPath:@"@max.self"] floatValue];
    CGFloat lngMin = [[lngArray valueForKeyPath:@"@min.self"] floatValue];
    CGFloat lngMax = [[lngArray valueForKeyPath:@"@max.self"] floatValue];
    if (coordinate.latitude < latMin || coordinate.latitude > latMax || coordinate.longitude < lngMin || coordinate.longitude > lngMax) {
        // We're outside the polygon!
        return NO;
    }
    
    int Num = latArray.count;
    //copy code 
    int i, j, c = 0;
    for (i = 0, j = Num - 1; i < Num; j = i++) {
        CGFloat latI = [[latArray objectAtIndex:i] floatValue];
        CGFloat latJ = [[latArray objectAtIndex:j] floatValue];
        CGFloat lngI = [[lngArray objectAtIndex:i] floatValue];
        CGFloat lngJ = [[lngArray objectAtIndex:j] floatValue];
        if ( ((lngI >coordinate.longitude) != (lngJ>coordinate.longitude)) &&
            (coordinate.latitude < (latJ-latI) * (coordinate.longitude-lngI) / (lngJ-lngI) + latI)) {
            c = !c;
        }
    }
    //NSLog(@"loop end c %i", c);
    return c;
}

#pragma mark - Request Location
void PUGetCityNameByCoord(CLLocationCoordinate2D coordinate, NSString *locale, DDBlockObject completion)
{
     NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSString *path = nil;
    if (GetAppDelegate().isChina) {
        path= [NSString stringWithFormat:@"geo?hl=%@&output=json&q=%f,%f", language, coordinate.latitude, coordinate.longitude];
    }else {
        path = [NSString stringWithFormat:@"api/geocode/json?latlng=%f,%f&sensor=true", coordinate.latitude, coordinate.longitude];
    }
    //百度接口
//    NSString *localtionStr = [NSString stringWithFormat:@"%f,%f", coordinate.latitude, coordinate.longitude];
//    NSString *path = [NSString stringWithFormat:@"geocoder?location=%@&output=json&key=%@", localtionStr, baiduMapKey];
    NSURL *url = PUMakeMapURL(path);
    NSLog(@"url is %@", url.absoluteString);

    NSURLRequest *request = PUMakeMapURLRequest(url);
    static AFJSONRequestOperation *staticOperation = nil;
    [staticOperation cancel];
    staticOperation = [[AFJSONRequestOperation alloc] initWithRequest:request];
    [staticOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *locality = nil;
        NSString *administrativeArea = nil;
        NSString *name = nil;
        if (GetAppDelegate().isChina) {
            TAPlaceMarkModel *placemark = [TAPlaceMarkModel placemarkWithDictionary:responseObject];
            name = placemark.locality? placemark.locality: placemark.administrativeArea;
            if (name && name.length &&[name hasSuffix:@"区"]) {
                name = placemark.administrativeArea;
            }
        }else {
            NSArray *results = [responseObject objectForKey:@"results"];
            NSString *status = [responseObject valueForKey:@"status"];
            
            if ([status isEqualToString:@"OK"]) {
                if(results.count > 0) {
                    for (id obj in results) {
                        SVPlacemark *placemark = [[SVPlacemark alloc] initWithDictionary:obj];
                        if (placemark.locality) {
                            locality = placemark.locality;
                            break;
                        }
                        if (!administrativeArea && placemark.administrativeArea) {
                            administrativeArea = placemark.administrativeArea;
                        }
                    }
                }
            }
            name = locality? locality: administrativeArea;
        }
        
        if (completion) {
            completion(name);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"request address failed %@", error.localizedDescription);
        if (completion) {
            completion(nil);
        }
    }];
    [staticOperation start];
}

NSURL* PUMakeMapURL(NSString *path)
{
    NSString *urlEncodingPath = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *serverHost = GetAppDelegate().isChina? MAP_SERVER_HOST_CN: MAP_SERVER_HOST;
    NSString *urlString = [serverHost stringByAppendingString:urlEncodingPath];
    return [NSURL URLWithString:urlString];
}

NSURLRequest* PUMakeMapURLRequest(NSURL *url)
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
    [request setHTTPShouldHandleCookies:NO];
    [request setHTTPShouldUsePipelining:YES];
    return request;
}