//
//  TAFullViewController.m
//  TravelAlbum
//
//  Created by Static Ga on 13-2-6.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import "TAFullViewController.h"
#import "NIPagingScrollView.h"
#import "NIPagingScrollViewDataSource.h"
#import "NIPagingScrollViewDelegate.h"
#import "TAFullPageView.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface TAFullViewController ()<NIPagingScrollViewDataSource,NIPagingScrollViewDelegate>
@property (nonatomic, strong) NIPagingScrollView *scrollView;
@property (nonatomic, strong) NSArray *photosArray;
@end

@implementation TAFullViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithPhotos :(NSArray *)photos {
    if (self = [super init]) {
        self.photosArray = photos;
    }
    return self;
}

- (void)loadView {
    [super loadView];
    self.view.backgroundColor = [UIColor clearColor];
    self.view.frame = [[UIScreen mainScreen] bounds];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _scrollView = [[NIPagingScrollView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    _scrollView.dataSource = self;
    _scrollView.delegate = self;
    [_scrollView reloadData];
    [self.view addSubview:_scrollView];

}

#pragma mark - dataSource
- (NSInteger)numberOfPagesInPagingScrollView:(NIPagingScrollView *)pagingScrollView {
    return self.photosArray.count;
}
- (UIView<NIPagingScrollViewPage> *)pagingScrollView:(NIPagingScrollView *)pagingScrollView pageViewForIndex:(NSInteger)pageIndex {
    static NSString *identifier = @"scrollIdentifier";
    TAFullPageView *pageView = (TAFullPageView *)[pagingScrollView dequeueReusablePageWithIdentifier:identifier];
    if (!pageView) {
        pageView = [[TAFullPageView alloc] initWithFrame:self.view.bounds];
    }
    if (![pageView haveImage]) {
        
    __block TAFullViewController *weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @autoreleasepool {
            ALAsset *asset = [weakSelf.photosArray objectAtIndex:pageIndex];
            ALAssetRepresentation *representation = [asset defaultRepresentation];
            CGImageRef imageReference = CGImageRetain([representation fullScreenImage]);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIImage *image = [UIImage imageWithCGImage:imageReference];
                [pageView setPageImage:image];
                CGImageRelease(imageReference);
            });
        }
    });
    }
    return pageView;
}

- (void)pagingScrollViewDidScroll:(NIPagingScrollView *)pagingScrollView {
    NSLog(@"scrollview scroll");
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
