//
//  TAConstantDefine.h
//  TravelAlbum
//
//  Created by rumble on 13-1-28.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//



//Map Server
extern NSString *MAP_SERVER_HOST;
extern NSString *MAP_SERVER_HOST_CN;
extern NSString const *baiduMapKey;
extern NSString *kPlacemarkCountryCN;
extern NSString *kPlacemarkCountryEN;
//Notification Name
extern NSString * const kNotificationGetCityName;
extern NSString * const kNotificationDeletePhotoCity;
extern NSString * const kNotificationEnumerateAllPhotos;
extern NSString * const kNotificationReadPhotoFailed;
extern NSString * const kNotificationReadSuccess;
extern NSString * const kNotificationRefreshUIFromDB;
extern NSString * const kNotificationAssetsDicFinished;
extern NSString * const kNotificationReEnumerateAllPhotos;
//DB Name
extern NSString * const DBUser;
extern NSString * const DBTravelAlbumCity;
//DBTravelAlbumCity Table Name
extern NSString * const DBCoord2CityidTable;
extern NSString * const DBPointInPolygonTable;
extern NSString * const DBCityid2CityNameTable;
//DBUser Table Name
extern NSString * const DBCity2AssetURLTable;
extern NSString * const DBAssetURLsTable;
//创建表
extern NSString * const DBSchemaKeyColumnName;
extern NSString * const DBSchemaKeyColumnDatatype;
extern NSString * const DBSchemaKeyColumnConstraint;

//字段
extern NSString * const DBCityID;
extern NSString * const DBCityName;
extern NSString * const DBLocationLat;
extern NSString * const DBLocationLng;
extern NSString * const DBPhotoAssetURL;
extern NSString * const DBPhotoAsset;
extern NSString * const DBphotoAssetURLs;
extern NSString * const DBphotoCreateDate;
extern NSString * const DBCityAreaNo;
extern NSInteger const DBUncategorizedCityID;
extern NSInteger const DBNoLocationMessageID;
extern NSInteger const DBGoogleCityID;
extern NSString * const DBUncategorizedCityName;
extern NSString * const DBNoLocationMessageName;

//数据库
extern NSString * const PXDBSeparator;
extern NSString * const PXDBPlaceholder;
extern NSString * const PXDBDatatypeNULL;
extern NSString * const PXDBDatatypeINTEGER;
extern NSString * const PXDBDatatypeREAL;
extern NSString * const PXDBDatatypeTEXT;
extern NSString * const PXDBDatatypeBLOB;

extern NSString * const PXDBConstraintPRIMARYKEY;
extern NSString * const PXDBConstraintUNIQUE;
extern NSString * const PXDBConstraintNOTNULL;
