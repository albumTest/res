//
//  TAUtility.h
//  TravelAlbum
//
//  Created by rumble on 13-1-29.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "DDType.h"
@class TAAppDelegate, FMDatabase;

BOOL isPointIn(void);

TAAppDelegate *GetAppDelegate(void);

NSData* PUArchiveObject(id obj);
id PUUnarchiveData(NSData *data);

void UnzipDBFile(void);

//TravelAlbum DB
FMDatabase *GetCityDB(void);
void EnumerateAllPhotosToDB(void);
NSDictionary *TAQueryCityNameByLocation(CLLocationCoordinate2D coordinate, FMDatabase *db);
NSDictionary *TAQueryCityNameByLocation_1(CLLocationCoordinate2D coordinate, FMDatabase *db);
void TAEndEnumeratePhotosWithWillChangedPhotos(NSMutableArray *savePhotosArray, NSMutableArray *deletePhotosArray);
void CreateUserDB(void);
NSArray* GetTableSchema(NSString *table);
void CreateTableSchema(FMDatabase *db, NSString *table, NSArray *schema);
void DBCreateIndex(FMDatabase *db, NSString *table, NSArray *columnNames, NSString *indexName);
NSString* DBGetIndexName(NSString *table, NSString *column);
NSString* DBMakeWhereClause(NSDictionary *keyColumns);
BOOL DBReadFromTable(NSString *table, NSDictionary *keyColumns, NSMutableDictionary *columns);
BOOL DBCheckQueryExist(FMDatabase *db, NSString *table, NSString *whereClause);
void DBWriteToUser(FMDatabase *db, NSString *table, NSDictionary *columns, DDBlockVoid completion);
void DBDeleteFromUser(FMDatabase *db, NSString *table, NSDictionary *columns, DDBlockVoid completion);
NSMutableDictionary *DBReadCityPolygon(FMDatabase *db);
NSMutableDictionary *DBReadCityNameByID(FMDatabase *db);
NSMutableDictionary *DBReadCityIDByLocation(FMDatabase *db);
void TAProcessUncategorizedPhotos(void);
void TARequestPhotoCityName(void);
void TAReCategorizePhoto(NSDictionary *columns);

dispatch_queue_t TAGetMainQueue(void);
dispatch_queue_t TAGetGlobalQueue(void);
void CheckQueue(dispatch_queue_t queue);
void CheckCurrentQueue(void);

NSNumber *TAMapCityLevel(id level);
BOOL isPointInPolygon(CLLocationCoordinate2D coordinate, NSMutableArray *latArray, NSMutableArray *lngArray);
NSDictionary *GetSmallCityDic(NSMutableArray *unSortArray);

void PUGetCityNameByCoord(CLLocationCoordinate2D coordinate, NSString *locale, DDBlockObject completion);
NSURL* PUMakeMapURL(NSString *path);
NSURLRequest* PUMakeMapURLRequest(NSURL *url);