//
//  TAThumbsViewController.m
//  TravelAlbum
//
//  Created by Static Ga on 13-2-1.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import "TAThumbsViewController.h"
#import "KTPhotoView.h"
#import "KTThumbView.h"
#import "UIView+Helper.h"
#import "TATool.h"

@interface TAThumbsViewController ()
@property (nonatomic, strong) NSArray *thumbsArray;
@property (nonatomic, strong) KTThumbsViewController *controller;
@end

@implementation TAThumbsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (id)initWithThumbs: (NSArray *)thumbs {
    if (self = [super init]) {
        self.thumbsArray = thumbs;
        scrollToBottom = YES;
    }
    return self;
}

//- (void)loadView {
//    [super loadView];
//    self.view.frame = [UIScreen mainScreen].applicationFrame;
//    self.view.backgroundColor = [UIColor clearColor];
//    NSLog(@"self.view frame %@",NSStringFromCGRect(self.view.frame));
//    self.view.origin = CGPointMake(0, 0);
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    KTThumbsViewController*cc = [[KTThumbsViewController alloc] init];
    cc.dataSource = self;
    self.controller = cc;
    [self.view addSubview:self.controller.view];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (scrollToBottom) {
        [self.controller scrollToBottom];
        scrollToBottom = NO;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfPhotos {
    return self.thumbsArray.count;
}

- (void)imageAtIndex:(NSInteger)index photoView:(KTPhotoView *)photoView {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSURL *url = [self.thumbsArray objectAtIndex:index];
        [[TATool assetsLibrary] assetForURL:url resultBlock:^(ALAsset *asset) {
            @autoreleasepool {
                ALAssetRepresentation *representation = [asset defaultRepresentation];
                CGImageRef imageReference = CGImageRetain([representation fullScreenImage]);
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIImage *image = [UIImage imageWithCGImage:imageReference];
                    [photoView setImage:image];
                    CGImageRelease(imageReference);
                });
            }
        } failureBlock:^(NSError *error) {
            NSLog(@"get image failed");
        }];
    });
    
    
}

- (void)thumbImageAtIndex:(NSInteger)index thumbView:(KTThumbView *)thumbView {
    __block TAThumbsViewController *weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *url = [weakSelf.thumbsArray objectAtIndex:index];
        [[TATool assetsLibrary] assetForURL:url resultBlock:^(ALAsset *asset) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [thumbView setThumbImage:[UIImage imageWithCGImage:asset.thumbnail]];
            });
        } failureBlock:^(NSError *error) {
            NSLog(@"get image failed");
        }];
    });
    
}
@end
