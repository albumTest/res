//
//  TAThumbsViewController.h
//  TravelAlbum
//
//  Created by Static Ga on 13-2-1.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import "KTThumbsViewController.h"

@interface TAThumbsViewController : UIViewController<KTPhotoBrowserDataSource>
{
    BOOL scrollToBottom;
}
- (id)initWithThumbs: (NSArray *)thumbs;
@end
