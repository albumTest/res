//
//  TAPlaceMarkModel.m
//  TravelAlbum
//
//  Created by Static Ga on 13-1-27.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import "TAPlaceMarkModel.h"

@implementation TAPlaceMarkModel
+ (id)placemarkWithDictionary:(NSDictionary *)dic {
    return [[self alloc] initWithDictionary:dic];
}
- (id)initWithDictionary:(NSDictionary *)dic {
    self = [super init];
    if (self) {
        self.address = @"";
        self.country = @"";
        self.countryCode = @"";
        self.administrativeArea = nil;
        self.locality = nil;
        self.dependentLocality = @"";
        self.thoroughfare = @"";
        
        self.statusCode = [[[dic objectForKey:@"Status"] objectForKey:@"code"] integerValue];
        
        if ([self statusIsOK]) {
#if 1
            [self recursiveParse:dic];
#else
            id placemarks = [dic objectForKey:@"Placemark"];
            
            id placemark = [placemarks objectAtIndex:0];
            self.address = [placemark objectForKey:@"address"];
            id addressDetails = [placemark objectForKey:@"AddressDetails"];
            
            self.accuracy = [[addressDetails objectForKey:@"Accuracy"] doubleValue];
            id country = [addressDetails objectForKey:@"Country"];
            
            self.country = [country objectForKey:@"CountryName"];
            self.countryCode = [country objectForKey:@"CountryNameCode"];
            id administrativeArea = [country objectForKey:@"AdministrativeArea"];
            
            self.administrativeArea = [administrativeArea objectForKey:@"AdministrativeAreaName"];
            id locality = [administrativeArea objectForKey:@"Locality"];
            
            self.locality = [locality objectForKey:@"LocalityName"];
            id dependentLocality = [locality objectForKey:@"DependentLocality"];
            
            self.dependentLocality = [dependentLocality objectForKey:@"DependentLocalityName"];
            id thoroughfare = [dependentLocality objectForKey:@"Thoroughfare"];
            
            self.thoroughfare = [thoroughfare objectForKey:@"ThoroughfareName"];
#endif
        }
    }
    
    return self;
}

- (id)init {
    self = [super init];
    if (self) {
        self.address = @"";
        self.country = @"";
        self.countryCode = @"";
        self.administrativeArea = @"";
        self.locality = @"";
        self.dependentLocality = @"";
        self.thoroughfare = @"";
        self.location = [CLLocation new];
        self.assetURL = [NSURL URLWithString:@""];
    }
    return self;
}


- (NSString *)diquComponents {
    NSArray *components = [NSArray arrayWithObjects:self.country, self.administrativeArea, self.locality, self.dependentLocality, self.thoroughfare, nil];
    return [components componentsJoinedByString:@","];
}
- (BOOL)isChina {
    return[self.countryCode isEqualToString:@"CN"];
}

- (BOOL)statusIsOK {
    return 200 == self.statusCode;
}

- (NSString *)cityName {
    /*if (self.dependentLocality.length > 0) {
     return self.dependentLocality;
     }else */if (self.locality.length > 0) {
         return self.locality;
     }else {
         return @"";
     }
}

- (NSString *)diquName {
    NSString *cityName = [self cityName];
    if (cityName.length > 0 && [cityName hasSuffix:@"市"]) {
        cityName = [cityName substringToIndex:cityName.length - 1];
    }
    return cityName;
}

- (void)recursiveParse:(id)collection {
    if ([collection isKindOfClass:[NSArray class]]) {
        NSEnumerator *objEnumerator = [collection objectEnumerator];
        id obj;
        while (obj = [objEnumerator nextObject]) {
            if ([obj isKindOfClass:[NSArray class]] || [obj isKindOfClass:[NSDictionary class]]) {
                [self recursiveParse:obj];
            }
        }
    }else if ([collection isKindOfClass:[NSDictionary class]]) {
        NSEnumerator *keyEnumerator = [collection keyEnumerator];
        NSString *key;
        while (key = [keyEnumerator nextObject]) {
            if ([key isEqualToString:@"address"]) {
                self.address = [collection objectForKey:@"address"];
            }else if ([key isEqualToString:@"CountryName"]) {
                self.country = [collection objectForKey:@"CountryName"];
            }else if ([key isEqualToString:@"CountryNameCode"]) {
                self.countryCode = [collection objectForKey:@"CountryNameCode"];
            }else if ([key isEqualToString:@"AdministrativeAreaName"]) {
                self.administrativeArea = [collection objectForKey:@"AdministrativeAreaName"];
            }else if ([key isEqualToString:@"LocalityName"]) {
                self.locality = [collection objectForKey:@"LocalityName"];
            }else if ([key isEqualToString:@"DependentLocalityName"]) {
                self.dependentLocality = [collection objectForKey:@"DependentLocalityName"];
            }else if ([key isEqualToString:@"ThoroughfareName"]) {
                self.thoroughfare = [collection objectForKey:@"ThoroughfareName"];
            }else {
                id obj = [collection objectForKey:key];
                if ([obj isKindOfClass:[NSArray class]] || [obj isKindOfClass:[NSDictionary class]]) {
                    [self recursiveParse:obj];
                }
            }
        }
    }
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%u : %@ : %@ : %@ : %@ : %@ : %@", self.statusCode, self.countryCode, self.country, self.administrativeArea, self.locality, self.dependentLocality, self.thoroughfare];
}

@end
