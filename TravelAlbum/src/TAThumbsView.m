//
//  TAThumbsView.m
//  TravelAlbum
//
//  Created by Static Ga on 13-2-5.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import "TAThumbsView.h"
#import "TATool.h"



@implementation TAThumbsView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)clearImage {
    [self setImage:nil forState:UIControlStateNormal];
}

- (dispatch_queue_t)sharedQueue {
    static dispatch_queue_t queue = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        queue = dispatch_queue_create("com.travelAlbum.cityQueue", 0);
    });
    return queue;
}
- (void)setImageWithURL:(NSURL *)url {
    if (url) {
        self.url = url;
        __block TAThumbsView *weakSelf = self;
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        dispatch_async(dispatch_get_main_queue(), ^{
        dispatch_async([self sharedQueue], ^{

        [[TATool assetsLibrary] assetForURL:url resultBlock:^(ALAsset *asset) {
                @autoreleasepool {
                NSDate *date = [asset valueForProperty:ALAssetPropertyDate];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"MMMdd日 HH:mm"];
                CGImageRef imageReference = CGImageRetain([asset thumbnail]);
                dispatch_async(dispatch_get_main_queue(), ^{
                    weakSelf.createDateString = [dateFormatter stringFromDate:date];
                    [self setImage:[UIImage imageWithCGImage:imageReference] forState:UIControlStateNormal];
//                    [weakSelf setImage:[UIImage imageWithCGImage:[asset thumbnail]] forState:UIControlStateNormal];
                    CFRelease(imageReference);
                    });
                }
            } failureBlock:^(NSError *error) {
                NSLog(@"get image failed");
            }];
        });

    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
