//
//  TAThumbsTableCell.m
//  TravelAlbum
//
//  Created by Static Ga on 13-2-5.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import "TAThumbsTableCell.h"
#import "TAThumbsView.h"
#import "UIView+Helper.h"
#import "TAThumbsTableViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>

CGFloat kSpace = 5.0f;

@interface TAThumbsTableCell ()
@property (nonatomic, strong) TAThumbsView *firstView;
@property (nonatomic, strong) TAThumbsView *secondView;
@property (nonatomic, strong) TAThumbsView *thirdView;

@property (nonatomic, strong) NSArray *viewsArray;
@end

@implementation TAThumbsTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.firstView = [TAThumbsView buttonWithType:UIButtonTypeCustom];
        self.firstView.size = CGSizeMake(100, 100);
        self.firstView.left = kSpace;
        self.firstView.top = kSpace;
        [self.contentView addSubview:self.firstView];
        
        self.secondView = [TAThumbsView buttonWithType:UIButtonTypeCustom];
        self.secondView.size = CGSizeMake(100, 100);
        self.secondView.left = kSpace + self.firstView.right;
        self.secondView.top = kSpace;
        [self.contentView addSubview:self.secondView];
        
        self.thirdView = [TAThumbsView buttonWithType:UIButtonTypeCustom];
        self.thirdView.size = CGSizeMake(100, 100);
        self.thirdView.left = kSpace + self.secondView.right;
        self.thirdView.top = kSpace;
        [self.contentView addSubview:self.thirdView];
        
        
        [self.firstView addTarget:self action:@selector(thumbViewSelected:) forControlEvents:UIControlEventTouchUpInside];
        [self.secondView addTarget:self action:@selector(thumbViewSelected:) forControlEvents:UIControlEventTouchUpInside];
        [self.thirdView addTarget:self action:@selector(thumbViewSelected:) forControlEvents:UIControlEventTouchUpInside];
        
        //        self.firstView.hidden = YES;
        //        self.secondView.hidden = YES;
        //        self.thirdView.hidden = YES;
        
        self.viewsArray = [NSArray arrayWithObjects:self.firstView, self.secondView, self.thirdView,nil];
        
    }
    return self;
}

- (void)thumbViewSelected :(id)sender {
    TAThumbsView *button = (TAThumbsView *)sender ;
    if (button.asset) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(thumbViewSeleceted:)]) {
            [self.delegate thumbViewSeleceted:button.asset];
        }
    }
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (void)setCellWithModels:(NSArray *)modelsArray {
    if (modelsArray) {
        for (int i = 0; i < self.viewsArray.count; i++) {
            TAThumbsView *thumbsView = [self.viewsArray objectAtIndex:i];
            //set
            thumbsView.hidden = YES;
//            [thumbsView clearImage];
            if (i < modelsArray.count) {
                thumbsView.hidden = NO;
//                [thumbsView setImageWithURL:[modelsArray objectAtIndex:i]];
                ALAsset *asset = [modelsArray objectAtIndex:i];
                [thumbsView setImage:[UIImage imageWithCGImage:[asset thumbnail]] forState:UIControlStateNormal];
                thumbsView.asset = asset;
            }
        }
        //        for (int i = count; i < self.viewsArray.count;++i) {
        //            TAThumbsView *view = [self.viewsArray objectAtIndex:i];
        //            view.hidden = YES;
        //        }
    }
}
@end
