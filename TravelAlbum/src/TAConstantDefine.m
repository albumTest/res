//
//  TAConstantDefine.m
//  TravelAlbum
//
//  Created by rumble on 13-1-28.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import "TAConstantDefine.h"

//Map Server
NSString *MAP_SERVER_HOST = @"http://maps.googleapis.com/maps/";//@"http://ditu.google.cn/maps/";//maps.googleapis.com/maps/
NSString *MAP_SERVER_HOST_CN = @"http://ditu.google.cn/maps/";
//NSString *MAP_SERVER_HOST = @"http://api.map.baidu.com/";
NSString const *baiduMapKey = @"5e8974b8ecbb13bcd82be41a04a4534d";
NSString *kPlacemarkCountryCN = @"zh-CN";
NSString *kPlacemarkCountryEN = @"zh-EN";

//Notification Name
NSString * const kNotificationGetCityName = @"TAGetPhotoCityName";
NSString * const kNotificationDeletePhotoCity = @"TADeletePhotoCity";
NSString * const kNotificationEnumerateAllPhotos = @"TAEnumerateAllPhotos";
NSString * const kNotificationReadPhotoFailed = @"TAReadphotoFailedNotification";
NSString * const kNotificationReadSuccess = @"TAReadphotoSuccessNotification";
NSString * const kNotificationRefreshUIFromDB = @"TARefreshUIFromDNotification";
NSString * const kNotificationAssetsDicFinished = @"TAAssetsDicFinishedNotification";
NSString * const kNotificationReEnumerateAllPhotos = @"TAReEnumerateAllPhotos";
//DB Name
NSString * const DBUser = @"user.db";
NSString * const DBTravelAlbumCity = @"TravelAlbum.db";
//DBTravelAlbumCity Table Name
NSString * const DBCoord2CityidTable = @"city_area_geo"; //@"geo_city_geo_1"; //
NSString * const DBPointInPolygonTable = @"city_area";
NSString * const DBCityid2CityNameTable = @"city";

//DBUser Table Name
NSString * const DBCity2AssetURLTable = @"city2AssetURL";
NSString * const DBAssetURLsTable = @"assetURLs";
//创建表
NSString * const DBSchemaKeyColumnName = @"column_name";
NSString * const DBSchemaKeyColumnDatatype = @"column_datatype";
NSString * const DBSchemaKeyColumnConstraint = @"column_constraint";
//字段
NSString * const DBCityID = @"cityID";
NSString * const DBCityName = @"cityName";
NSString * const DBLocationLat = @"lat";
NSString * const DBLocationLng = @"lng";
NSString * const DBPhotoAssetURL = @"assetURL";
NSString * const DBPhotoAsset = @"assets";
NSString * const DBphotoAssetURLs = @"assetURLs";
NSString * const DBphotoCreateDate = @"assetCreateDate";
NSString * const DBCityAreaNo = @"areaNo";
NSInteger const DBUncategorizedCityID = -1;
NSInteger const DBNoLocationMessageID = -2;
NSInteger const DBGoogleCityID = -3;
NSString * const DBUncategorizedCityName = @"Uncategorized";//@"未分类";
NSString * const DBNoLocationMessageName = @"无位置信息的图片";
//数据库
NSString * const PXDBSeparator = @", ";
NSString * const PXDBPlaceholder = @"?";
NSString * const PXDBDatatypeNULL = @"NULL";
NSString * const PXDBDatatypeINTEGER = @"INTEGER";
NSString * const PXDBDatatypeREAL = @"REAL";
NSString * const PXDBDatatypeTEXT = @"TEXT";
NSString * const PXDBDatatypeBLOB = @"BLOB";

NSString * const PXDBConstraintPRIMARYKEY = @"PRIMARY KEY";
NSString * const PXDBConstraintUNIQUE = @"UNIQUE";
NSString * const PXDBConstraintNOTNULL = @"NOT NULL";