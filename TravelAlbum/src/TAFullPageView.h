//
//  TAFullPageView.h
//  TravelAlbum
//
//  Created by Static Ga on 13-2-6.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import "NIPageView.h"

@interface TAFullPageView : NIPageView
- (void)setPageImage:(UIImage *)image;
- (BOOL)haveImage;
@end
