//
//  TAListCell.m
//  TravelAlbum
//
//  Created by rumble on 13-1-27.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import "TAListCell.h"
#import "TAListItem.h"
#import "TATool.h"
#import "UIView+Helper.h"
static const CGFloat PhotoWidth = 60;
const CGFloat cellHeight = 70;
@interface TAListCell ()
@end

@implementation TAListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellEditingStyleNone;
        // Initialization code
        CGFloat margin = (cellHeight - PhotoWidth) / 2;
        _coverView = [[UIImageView alloc] initWithFrame:CGRectMake(margin, margin, PhotoWidth, PhotoWidth)];
        _coverView.backgroundColor = [UIColor grayColor];
        
        _cityName = [UILabel new];
        [_cityName setTextColor:RGBCOLOR(80, 80, 80)];
        _cityName.font = [UIFont boldSystemFontOfSize:18];
        
        _photosCount = [UILabel new];
        [_photosCount setTextColor:[UIColor grayColor]];
        _photosCount.font = [UIFont systemFontOfSize:18];
        
        _arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ico_arrow"]];
        [_arrow sizeToFit];
        
        _cityName.backgroundColor = [UIColor clearColor];
        _photosCount.backgroundColor = [UIColor clearColor];
        
        [self addSubview:_coverView];
        [self addSubview:_cityName];
        [self addSubview:_photosCount];
        [self addSubview:_arrow];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellwithModel:(id)model {
    
    TAListItem *item = model;
    //NSLog(@"TAListCell setCellwithModel");
    _cityName.text = item.cityName;
    _photosCount.text = [NSString stringWithFormat:@"（%i）",item.photosCount] ;
    [_cityName sizeToFit];
    if (_cityName.width > 130) {
        _cityName.width = 130;
    }
    //NSLog(@"the cityName size %@",NSStringFromCGSize(_cityName.size));
    _cityName.left = _coverView.right + 10;
    _cityName.centerY = _coverView.centerY;
    
    [_photosCount sizeToFit];
    _photosCount.left = _cityName.right + 5;
    _photosCount.centerY = _coverView.centerY;
    
    _arrow.right = self.width - 10;
    _arrow.centerY = _coverView.centerY;
    
   [[TATool assetsLibrary] assetForURL:item.coverViewURL resultBlock:^(ALAsset *asset) {
       //_coverView.hidden = NO;
       _coverView.image = [UIImage imageWithCGImage:asset.thumbnail]; 
   } failureBlock:^(NSError *error) {
       NSLog(@"get image failed");
       //_coverView.hidden = YES;
   }];
}
@end
