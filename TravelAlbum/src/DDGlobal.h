//
//  DDGlobal.h
//  paixiu
//
//  Created by darkdong on 12-3-16.
//  Copyright (c) 2012年 PaiXiu. All rights reserved.
//

#ifndef DDGlobal_h
#define DDGlobal_h

#ifdef DEBUG
#define DD_RELEASE(__POINTER) { [__POINTER release]; }
#else
#define DD_RELEASE(__POINTER) { [__POINTER release]; __POINTER = nil; }
#endif

#define DD_INVALIDATE_TIMER(__TIMER) { [__TIMER invalidate]; __TIMER = nil; }
#define DD_RELEASE_SAFELY(__POINTER) { [__POINTER release]; __POINTER = nil; }

#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]
#define RGBACOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f \
alpha:(a)]

#if defined(__has_feature) && __has_feature(objc_arc)
    #if __has_feature(objc_arc_weak)
//        #error ARC and WEAK
        #define MONITOR_UNRETAINED_BY_BLOCK __weak
        #define MONITOR_ADD_SELF
        #define MONITOR_REMOVE_SELF
        #define MONITOR_CHECK_UNRETAINED_SELF (unretainedSelf)
    #else
//        #error ARC but not WEAK
        #define MONITOR_UNRETAINED_BY_BLOCK __unsafe_unretained
        #define MONITOR_ADD_SELF [PaiXiu addObjectToMonitor:self.hash];
        #define MONITOR_REMOVE_SELF [PaiXiu removeObjectFromMonitor:self.hash];
        #define MONITOR_CHECK_UNRETAINED_SELF ([PaiXiu isObjectAlive:selfHashCode])
    #endif
#else
//    #error not ARC
    #define MONITOR_UNRETAINED_BY_BLOCK __block
    #define MONITOR_ADD_SELF [PaiXiu addObjectToMonitor:self.hash];
    #define MONITOR_REMOVE_SELF [PaiXiu removeObjectFromMonitor:self.hash];
    #define MONITOR_CHECK_UNRETAINED_SELF ([PaiXiu isObjectAlive:selfHashCode])
#endif

#define MONITOR_MAKE_UNRETAINED_SELF \
id MONITOR_UNRETAINED_BY_BLOCK unretainedSelf = self; \
NSUInteger selfHashCode = self.hash;

#endif
