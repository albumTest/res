//
//  TAThumbsTableCell.h
//  TravelAlbum
//
//  Created by Static Ga on 13-2-5.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>

@protocol TAThumbViewSelectedDelegate;

@interface TAThumbsTableCell : UITableViewCell

@property (nonatomic, assign) id<TAThumbViewSelectedDelegate>delegate;

- (void)setCellWithModels:(NSArray *)modelsArray;
@end

@protocol TAThumbViewSelectedDelegate <NSObject>

- (void)thumbViewSeleceted:(ALAsset *)asset;

@end