//
//  TAThumbsTableViewController.h
//  TravelAlbum
//
//  Created by Static Ga on 13-2-5.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TAThumbsTableViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *thumbTableView;
}

- (id)initWithThumbs:(NSArray *)thumbs title:(NSString *)topTitle;
@end
