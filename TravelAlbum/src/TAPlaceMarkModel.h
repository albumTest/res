//
//  TAPlaceMarkModel.h
//  TravelAlbum
//
//  Created by Static Ga on 13-1-27.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface TAPlaceMarkModel : NSObject
@property NSUInteger statusCode;
@property(nonatomic, retain) NSString *address;
@property(nonatomic, retain) NSString *country;
@property(nonatomic, retain) NSString *countryCode;
@property(nonatomic, retain) NSString *administrativeArea;
@property(nonatomic, retain) NSString *locality;
@property(nonatomic, retain) NSString *dependentLocality;
@property(nonatomic, retain) NSString *thoroughfare;
@property(nonatomic, retain) CLLocation *location;
@property(nonatomic, retain) NSURL *assetURL;

+ (id)placemarkWithDictionary:(NSDictionary *)dic;
- (id)initWithDictionary:(NSDictionary *)dic;
- (NSString *)diquComponents;
- (BOOL)isChina;
- (BOOL)statusIsOK;
- (NSString *)cityName;
- (NSString *)diquName;
@end
