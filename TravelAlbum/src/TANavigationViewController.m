//
//  TANavigationViewController.m
//  TravelAlbum
//
//  Created by Static Ga on 13-2-6.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import "TANavigationViewController.h"
#import "KTPhotoScrollViewController.h"

@interface TANavigationViewController ()

@end

@implementation TANavigationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (BOOL)shouldAutorotate {
    NSLog(@"shouldAutorotate");
    return YES;
}

-(NSInteger)supportedInterfaceOrientations{
    NSLog(@"supported ");
    if([[self topViewController] isKindOfClass:[KTPhotoScrollViewController class]])
        return UIInterfaceOrientationMaskAll;
    else
        return UIInterfaceOrientationMaskPortrait;

}

@end
