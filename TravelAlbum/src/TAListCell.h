//
//  TAListCell.h
//  TravelAlbum
//
//  Created by rumble on 13-1-27.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TAListCell : UITableViewCell
@property (nonatomic, strong) UIImageView *coverView;
@property (nonatomic, strong) UILabel *cityName;
@property (nonatomic, strong) UILabel *photosCount;
@property (nonatomic, strong) UIImageView *arrow;

- (void)setCellwithModel:(id)model;
@end
