//
//  TAFullPageView.m
//  TravelAlbum
//
//  Created by Static Ga on 13-2-6.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import "TAFullPageView.h"
#import "UIView+Helper.h"

@interface TAFullPageView ()
@property (nonatomic, strong) UIImageView *imageView;
@end

@implementation TAFullPageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        _imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_imageView];
    }
    return self;
}

- (BOOL)haveImage {
    return nil == _imageView.image;
}
#pragma mark - 
#pragma mark - public

- (void)setPageImage:(UIImage *)image {
    if (image) {
        _imageView.image = image;
//        [_imageView sizeToFit];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
