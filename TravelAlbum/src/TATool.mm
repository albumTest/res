//
//  TATool.m
//  TravelAlbum
//
//  Created by Static Ga on 13-1-27.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import "TATool.h"
#import <CoreLocation/CoreLocation.h>
#import "JSONKit.h"
#import "TAPlaceMarkModel.h"
#import "AFJSONRequestOperation.h"
#import <QuartzCore/QuartzCore.h>
#import "JSONKit.h"
#import "NSObject+Helper.h"
#import "TAAppDelegate.h"
#import "TAConstantDefine.h"
#import "TAUtility.h"
#import "FMDatabase.h"
#import "UIView+Helper.h"

static const NSTimeInterval kRequestPlacemarkTimeout = 30;

static dispatch_queue_t ta_city_request_operation_processing_queue;
static dispatch_queue_t ta_getCity_request_operation_processing_queue;
static dispatch_queue_t ta_db_operation_processing_queue;
static dispatch_queue_t ta_save_to_userDB_operation_processing_queue;
@interface TATool ()

@end

@implementation TATool

#if 1

+ (void)test {
    dispatch_async([TATool cityQueue], ^{
        
    
    [[TATool assetsLibrary] enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
       [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
           
           NSString *assetType = [result valueForProperty:ALAssetPropertyType];
           if ([assetType isEqualToString:ALAssetTypePhoto]) {
               //photo
               //NSLog(@"photo");
           }else if ([assetType isEqualToString:ALAssetTypeVideo]) {
               //video
           }
           //get url
           NSDictionary *assetURLs = [result valueForProperty:ALAssetPropertyURLs];
           NSURL *assetURL = nil;
           NSUInteger assetCounter = 0;
           for (NSString *assetURLKey in assetURLs) {
               assetCounter++;
               //NSLog(@"Asset URL %lu = %@",(unsigned long)assetCounter,[assetURLs valueForKey:assetURLKey]);
               assetURL = [assetURLs valueForKey:assetURLKey];
               if (assetURL) {
                   break;
               }
           }
           
           //get location
           CLLocation *location = [result valueForProperty:ALAssetPropertyLocation];
           if (location && CLLocationCoordinate2DIsValid(location.coordinate)) {
//               TAPlaceMarkModel *placemarkCN;
//               TAPlaceMarkModel *placemarkEN;
//               PUGetPlacemarkByLocale(location.coordinate, kPlacemarkCountryCN);
//               PUGetPlacemark(location.coordinate, &placemarkCN, &placemarkEN);
//               NSLog(@"placeCN %@",placemarkCN);

           }
            //get representation
//           ALAssetRepresentation *assetRepresentation = [result defaultRepresentation];
       }];
    } failureBlock:^(NSError *error) {
        NSLog(@"failed");
    }];
    });
}

#pragma mark - 
#pragma mark - queue

+ (dispatch_queue_t)cityQueue {
    if (NULL == ta_city_request_operation_processing_queue) {
        ta_city_request_operation_processing_queue = dispatch_queue_create("com.travelAlbum.cityQueue", 0);
    }
    return ta_city_request_operation_processing_queue;
}

+ (dispatch_queue_t)getCityQueue {
    if (NULL == ta_getCity_request_operation_processing_queue) {
        ta_getCity_request_operation_processing_queue = dispatch_queue_create("com.travelAlbum.getCityQueue", 0);
    }
    return ta_getCity_request_operation_processing_queue;
}

+ (dispatch_queue_t)dbQueue {
    if (NULL == ta_db_operation_processing_queue) {
        ta_db_operation_processing_queue = dispatch_queue_create("com.travelAlbum.dbQueue", 0);
    }
    return ta_db_operation_processing_queue;
}

+ (dispatch_queue_t)saveToUserDBQueue {
    if (NULL == ta_save_to_userDB_operation_processing_queue) {
        ta_save_to_userDB_operation_processing_queue = dispatch_queue_create("com.travelAlbum.saveToUserDBQueue", 0);
    }
    return ta_save_to_userDB_operation_processing_queue;
}

#pragma mark -
#pragma mark - location

NSURL* PUMakeMapURL(NSString *path)
{
    NSString *urlEncodingPath = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *urlString = [MAP_SERVER_HOST stringByAppendingString:urlEncodingPath];
    return [NSURL URLWithString:urlString];
}

NSURLRequest* PUMakeMapURLRequest(NSURL *url)
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
    [request setHTTPShouldHandleCookies:NO];
    [request setHTTPShouldUsePipelining:YES];
    return request;
}

NSDictionary* PUGetPlacemarkByLocale(CLLocationCoordinate2D coordinate, NSString *locale)
{
    NSString *path = [NSString stringWithFormat:@"geo?hl=%@&output=json&q=%f,%f", locale, coordinate.latitude, coordinate.longitude];
    NSURL *url = PUMakeMapURL(path);
    NSURLRequest *request = PUMakeMapURLRequest(url);
    NSURLResponse *response;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:NULL];
    if (!data) {
        return nil;
    }
    id obj = [[JSONDecoder decoder] objectWithData:data];
    if (!obj) {
        return nil;
    }
    NSLog(@"obj iss %@",obj);
    if ([locale isEqualToString:kPlacemarkCountryCN]) {
        NSMutableDictionary *finalResult = [NSMutableDictionary dictionary];
        TAPlaceMarkModel *placemarkCN = [TAPlaceMarkModel placemarkWithDictionary:obj];
        [finalResult setObject:placemarkCN forKey:locale];
        if (!placemarkCN.isChina) {
            NSDictionary *placemarkENDic = PUGetPlacemarkByLocale(coordinate, kPlacemarkCountryEN);
            [finalResult addEntriesFromDictionary:placemarkENDic];
        }
        return finalResult;
    }else {
        TAPlaceMarkModel *placemarkEN = [TAPlaceMarkModel placemarkWithDictionary:obj];
        return [NSDictionary dictionaryWithObject:placemarkEN forKey:locale];
    }
}

void PUGetPlacemark(CLLocationCoordinate2D coordinate, TAPlaceMarkModel **placemarkCN, TAPlaceMarkModel **placemarkEN)
{
    NSDictionary *placemarks = PUGetPlacemarkByLocale(coordinate, kPlacemarkCountryCN);
    *placemarkCN = [placemarks objectForKey:kPlacemarkCountryCN];
    *placemarkEN = [placemarks objectForKey:kPlacemarkCountryEN];
}


#pragma mark -
#pragma mark - sharedTool

+ (TATool *)sharedTool {
    static TATool *sharedTool ;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedTool = [[TATool alloc] init];
    });
    return sharedTool;
}

+ (ALAssetsLibrary *)assetsLibrary {
    static ALAssetsLibrary *sharedLibrary = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedLibrary = [[ALAssetsLibrary alloc] init];
    });
    return sharedLibrary;
}

+ (FMDatabase *)sharedCoord2CityNameDB {
    static FMDatabase *sharedCoord2CityNameDB;
    static dispatch_once_t onceToken;
#if 1
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *path = [docsPath stringByAppendingPathComponent:@"TravelAlbum.db"];
#else
    NSString *path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"TravelAlbum.db"];
#endif
    dispatch_once(&onceToken, ^{
        sharedCoord2CityNameDB = [FMDatabase databaseWithPath:path];
    });
    return sharedCoord2CityNameDB;
}

+ (FMDatabase *)sharedUserDB {
    static FMDatabase *sharedUserDB;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedUserDB = [FMDatabase databaseWithPath:[TATool shareUserDBFilePath]];
    });
    return sharedUserDB;
}

+ (NSString*)shareUserDBFilePath
{
    static NSString *shareUserDBFilePath = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *libraryDir = [paths objectAtIndex:0];
        shareUserDBFilePath = [[NSString alloc] initWithString:[libraryDir stringByAppendingPathComponent:@"user.db"]];
    });
    return shareUserDBFilePath;
}
#pragma mark - 
#pragma mark - private

#endif

#pragma mark -
#pragma mark - UI

#define kBarButtonTag 1021
+ (UIView *)makeBarTitleImage:(NSString *)titleImageName numberCount:(NSInteger)photoTypeCount {
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:titleImageName]];
    [imageView sizeToFit];

    imageView.centerX = 320/2;
    imageView.top = 0;

    UIButton *titleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [titleButton setTitle:[NSString stringWithFormat:@"%i",photoTypeCount] forState:UIControlStateNormal];
    [titleButton setBackgroundImage:[[UIImage imageNamed:@"bg_nums"] stretchableImageWithLeftCapWidth:20 topCapHeight:0] forState:UIControlStateNormal];
    titleButton.tag = kBarButtonTag;
    [titleButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0.0, 0.0)];
    [titleButton sizeToFit];
    titleButton.left = imageView.right + 10;
    titleButton.centerY = imageView.centerY;
    
    UIView *container = [[UIView alloc] initWithFrame:CGRectZero];
    container.backgroundColor = [UIColor clearColor];
    container.userInteractionEnabled = NO;
    
    [container addSubview:imageView];
    [container addSubview:titleButton];
     
    return container;
}

@end
