//
//  TATool.h
//  TravelAlbum
//
//  Created by Static Ga on 13-1-27.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "DDType.h"
@class TAAppDelegate, FMDatabase;
@interface TATool : NSObject
+ (void)test ;
+ (dispatch_queue_t)cityQueue ;
+ (dispatch_queue_t)getCityQueue ;
+ (dispatch_queue_t)dbQueue;
+ (dispatch_queue_t)saveToUserDBQueue;

+ (TATool *)sharedTool ;
+ (ALAssetsLibrary *)assetsLibrary;
+ (FMDatabase *)sharedCoord2CityNameDB;
+ (FMDatabase *)sharedUserDB;
+ (NSString*)shareUserDBFilePath;

+ (UIView *)makeBarTitleImage:(NSString *)titleImageName numberCount:(NSInteger)photoTypeCount;
@end
