//
//  DDHost.h
//  lvtu
//
//  Created by 雪松 董 on 11-11-22.
//  Copyright (c) 2011年 lvtu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DDHost : NSObject

+ (NSString *)nonLANIPAddress;
+ (NSArray *)addressesForHostname:(NSString *)hostname;
+ (NSString *)addressForHostname:(NSString *)hostname;
+ (NSArray *)hostnamesForAddress:(NSString *)address;
+ (NSString *)hostnameForAddress:(NSString *)address;
+ (NSArray *)ipAddresses;
+ (NSArray *)ethernetAddresses;

@end
