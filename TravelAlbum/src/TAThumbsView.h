//
//  TAThumbsView.h
//  TravelAlbum
//
//  Created by Static Ga on 13-2-5.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface TAThumbsView : UIButton
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) NSString *createDateString;
@property (nonatomic, strong) ALAsset *asset;
- (void)setImageWithURL:(NSURL *)url;
- (void)clearImage;
@end
