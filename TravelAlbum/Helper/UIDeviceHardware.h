//
//  UIDeviceHardware.h
//  Lepai
//
//  Created by 雪松 董 on 11-10-20.
//  Copyright (c) 2011年 Lepai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIDeviceHardware : NSObject

+ (NSString *)deviceName;
+ (NSString *)deviceVersion;
- (NSString *)platform;
- (NSString *)platformString;

@end
