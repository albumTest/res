//
//  UIButton+Helper.m
//  paixiu
//
//  Created by darkdong on 12-10-3.
//  Copyright (c) 2012年 PaiXiu. All rights reserved.
//

#import "UIButton+Helper.h"

@implementation UIButton (Helper)

- (void)cancelTracking {
    [self cancelTrackingWithEvent:nil];
}

- (void)setCancelTrackingWhenDraggingInside:(BOOL)cancelTrackingWhenDraggingInside {
    if (cancelTrackingWhenDraggingInside) {
        [self addTarget:self action:@selector(cancelTracking) forControlEvents:UIControlEventTouchDragInside];
    }else {
        [self removeTarget:self action:@selector(cancelTracking) forControlEvents:UIControlEventTouchDragInside];
    }
}

@end
