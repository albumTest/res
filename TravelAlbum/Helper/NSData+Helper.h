//
//  NSData+Helper.h
//  paixiu
//
//  Created by darkdong on 12-2-15.
//  Copyright (c) 2012年 PaiXiu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCryptor.h>

@interface NSData (Helper)
- (NSString*)stringWithHexBytes;
+ (NSData *)desData:(NSData *)data key:(NSString *)keyString CCOperation:(CCOperation)op;
- (NSString *)stringWithBytes;
- (Float64)extractFloat64AtOffset:(NSUInteger)offset;
- (UInt64)extractInt64FromBEAtOffset:(NSUInteger)offset;
- (UInt32)extractInt32FromBEAtOffset:(NSUInteger)offset;
- (UInt16)extractInt16FromBEAtOffset:(NSUInteger)offset;
@end
