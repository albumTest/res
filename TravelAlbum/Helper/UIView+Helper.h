//
//  UIView+Helper.h
//  paixiu
//
//  Created by darkdong on 12-2-29.
//  Copyright (c) 2012年 PaiXiu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DDType.h"

@interface UIView (Helper)

- (void)autoRemoveAfterSeconds:(NSTimeInterval)seconds animated:(BOOL)animated completion:(DDBlockVoid)completion;
- (void)autoRemovedAfterSeconds:(NSTimeInterval)seconds;
- (void)removeFromSuperviewAnimated:(BOOL)animated completion:(DDBlockVoid)completion;
- (void)shakeToShow;
- (void)shakeOut;
- (UIViewController*)viewController;
- (UITableView *)subTableView;
- (UIScrollView *)subScrollView;
- (UIView *)subviewWithClass:(Class)viewClass;
- (void)removeAllSubviews;
- (void)removeSubviewsByClass:(Class)viewClass;
- (void)removeMBProgressHUDSubviews;
- (void)disableScrollsToTopPropertyOnAllSubviews;
- (UIView*)findFirstResponder;
- (void)setHeightWithoutChangingBottom:(CGFloat)height;
+ (void)layoutSubviewsHorizontally:(NSArray *)subviews insets:(UIEdgeInsets)insets verticalAlignment:(UIControlContentVerticalAlignment)verticalAlignment inFrame:(CGRect)frame;
- (void)layoutSubviewsHorizontallyWithInsets:(UIEdgeInsets)insets verticalAlignment:(UIControlContentVerticalAlignment)verticalAlignment inFrame:(CGRect)frame;
- (void)layoutSubviewsHorizontallyWithInsets:(UIEdgeInsets)insets verticalAlignment:(UIControlContentVerticalAlignment)verticalAlignment;
+ (void)layoutSubviewsVertically:(NSArray *)subviews insets:(UIEdgeInsets)insets horizontalAlignment:(UIControlContentHorizontalAlignment)horizontalAlignment inFrame:(CGRect)frame;
- (void)layoutSubviewsVerticallyWithInsets:(UIEdgeInsets)insets horizontalAlignment:(UIControlContentHorizontalAlignment)horizontalAlignment inFrame:(CGRect)frame;
- (void)layoutSubviewsVerticallyWithInsets:(UIEdgeInsets)insets horizontalAlignment:(UIControlContentHorizontalAlignment)horizontalAlignment;
+ (void)layoutSubviewsGriddly:(NSArray *)subviews columns:(NSUInteger)columns gridSize:(CGSize)gridSize inFrame:(CGRect)frame;
- (void)layoutSubviewsGriddlyWithColumns:(NSUInteger)columns gridSize:(CGSize)gridSize insets:(UIEdgeInsets)insets;
- (void)layoutSubviewsGriddlyWithColumns:(NSUInteger)columns gridSize:(CGSize)gridSize;
/**
 * Shortcut for frame.origin.x.
 *
 * Sets frame.origin.x = left
 */
@property (nonatomic) CGFloat left;

/**
 * Shortcut for frame.origin.y
 *
 * Sets frame.origin.y = top
 */
@property (nonatomic) CGFloat top;

/**
 * Shortcut for frame.origin.x + frame.size.width
 *
 * Sets frame.origin.x = right - frame.size.width
 */
@property (nonatomic) CGFloat right;

/**
 * Shortcut for frame.origin.y + frame.size.height
 *
 * Sets frame.origin.y = bottom - frame.size.height
 */
@property (nonatomic) CGFloat bottom;

/**
 * Shortcut for frame.size.width
 *
 * Sets frame.size.width = width
 */
@property (nonatomic) CGFloat width;

/**
 * Shortcut for frame.size.height
 *
 * Sets frame.size.height = height
 */
@property (nonatomic) CGFloat height;

/**
 * Shortcut for center.x
 *
 * Sets center.x = centerX
 */
@property (nonatomic) CGFloat centerX;

/**
 * Shortcut for center.y
 *
 * Sets center.y = centerY
 */
@property (nonatomic) CGFloat centerY;

/**
 * Shortcut for frame.origin
 */
@property (nonatomic) CGPoint origin;

/**
 * Shortcut for frame.size
 */
@property (nonatomic) CGSize size;

@end
