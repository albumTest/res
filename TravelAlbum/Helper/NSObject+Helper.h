//
//  NSObject+Helper.h
//  paixiu
//
//  Created by darkdong on 12-8-10.
//  Copyright (c) 2012年 PaiXiu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Helper)

@property (nonatomic, retain) id associatedObject;

@end
