//
//  NSMutableDictionary+Helper.m
//  paixiu
//
//  Created by darkdong on 12-5-23.
//  Copyright (c) 2012年 PaiXiu. All rights reserved.
//

#import "NSMutableDictionary+Helper.h"
#import "NSString+NimbusCore.h"

static NSString *kRenRenAppKey = @"fb00fec390dd4cfc9026757b6522a592";
static NSString *kRenRenAppSecret = @"0cf5a80419ea4959978caa0e93c85942";

@implementation NSMutableDictionary (Helper)

- (void)signByRenRen {
//    [self setObject:token forKey:@"access_token"];
    [self setObject:@"1.0" forKey:@"v"];
    [self setObject:@"JSON" forKey:@"format"];
    
    NSMutableArray *parameters = [NSMutableArray arrayWithCapacity:self.count];
    [self enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        NSString *parameter = [NSString stringWithFormat:@"%@=%@", key, obj];
        [parameters addObject:parameter];
    }];
    NSLog(@"parameters %@", parameters);
    NSArray *sortedParameters = [parameters sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [obj1 compare:obj2];
    }];
    NSLog(@"sortedParameters %@", sortedParameters);
    
    NSString *strWithoutSecret = [sortedParameters componentsJoinedByString:@""];
    NSLog(@"strWithoutSecret %@", strWithoutSecret);
    NSString *strWithSecret = [strWithoutSecret stringByAppendingString:kRenRenAppSecret];
    NSLog(@"strWithSecret %@", strWithSecret);
    NSString *signature = [strWithSecret md5Hash];
    NSLog(@"signature %@", signature);
    [self setObject:signature forKey:@"sig"];
}

//- (NSURL *)generateRenRenRequestURLWithAccessToken:(NSString *)token {
//    NSString *signature = [self generateRenRenRequestURLWithAccessToken:token];
//    [self setObject:signature forKey:@"sig"];
//    
//    NSMutableString *requestURL = [NSMutableString stringWithString:@"http://api.renren.com/restserver.do?"];
//    [self enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
//        NSString *parameter = [NSString stringWithFormat:@"%@=%@", key, obj];
//        [requestURL appendFormat:@"&%@", parameter];
//    }];
//    PXLog(@"requestURL %@", requestURL);
//    return [NSURL URLWithString:requestURL];
//}

@end
