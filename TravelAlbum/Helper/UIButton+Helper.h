//
//  UIButton+Helper.h
//  paixiu
//
//  Created by darkdong on 12-10-3.
//  Copyright (c) 2012年 PaiXiu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Helper)

- (void)setCancelTrackingWhenDraggingInside:(BOOL)cancelTrackingWhenDraggingInside;

@end
