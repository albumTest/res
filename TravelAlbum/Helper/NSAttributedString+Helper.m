//
//  NSAttributedString+Helper.m
//  paixiu
//
//  Created by darkdong on 12-8-22.
//  Copyright (c) 2012年 PaiXiu. All rights reserved.
//

#import <CoreText/CoreText.h>
#import "NSAttributedString+Helper.h"
//#import "NSAttributedString+NimbusAttributedLabel.h"

@implementation NSAttributedString (Helper)

- (CGSize)sizeThatFits:(CGSize)size {
    CFAttributedStringRef attributedStringRef = (CFAttributedStringRef)self;
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(attributedStringRef);
	CFRange fitCFRange = CFRangeMake(0,0);
	CGSize newSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, 0),
                                                                  NULL, size, &fitCFRange);
    
	if (nil != framesetter) {
        CFRelease(framesetter);
        framesetter = nil;
    }
    
	return CGSizeMake(ceilf(newSize.width), ceilf(newSize.height));
}

@end
