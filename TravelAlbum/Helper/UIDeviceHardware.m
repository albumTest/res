//
//  UIDeviceHardware.m
//  Lepai
//
//  Created by 雪松 董 on 11-10-20.
//  Copyright (c) 2011年 Lepai. All rights reserved.
//

#import "UIDeviceHardware.h"
#include <sys/types.h>
#include <sys/sysctl.h>

const CGFloat kDeviceVersionIphone1G = 1.1;
const CGFloat kDeviceVersionIphone3G = 1.2;
const CGFloat kDeviceVersionIphone3GS = 2.1;
const CGFloat kDeviceVersionIphone4 = 3.1;
const CGFloat kDeviceVersionIphone4S = 4.1;

@implementation UIDeviceHardware

+ (NSString *)deviceName {
    UIDeviceHardware *device = [UIDeviceHardware new];
    return [device platformString];
}
+ (NSString *)deviceVersion {
    UIDeviceHardware *device = [UIDeviceHardware new];
    NSString *version = [[device platform] stringByTrimmingCharactersInSet:[NSCharacterSet letterCharacterSet]];
    version = [version stringByReplacingOccurrencesOfString:@"," withString:@"."];
    return version;
}
- (NSString *)platform{
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
    free(machine);
    return platform;
}

- (NSString *)platformString{
    NSString *platform = [self platform];
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([platform hasPrefix:@"iPhone5"])    return @"iPhone 5";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform hasPrefix:@"iPod5"])    return @"iPod Touch 5";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad mini";

    if ([platform hasPrefix:@"iPad3"])      return @"New iPad";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    return platform;
}

@end
