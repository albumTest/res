//
//  NSAttributedString+Helper.h
//  paixiu
//
//  Created by darkdong on 12-8-22.
//  Copyright (c) 2012年 PaiXiu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSAttributedString (Helper)

- (CGSize)sizeThatFits:(CGSize)size;

@end
