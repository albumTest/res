//
//  NSData+Helper.m
//  paixiu
//
//  Created by darkdong on 12-2-15.
//  Copyright (c) 2012年 PaiXiu. All rights reserved.
//

#import "NSData+Helper.h"

@implementation NSData (Helper)

//- (NSString *)stringWithBytes{
//    Byte *bytes = (Byte *)[self bytes];
//    //   NSString *hexStr=@"";
//    NSString *hexStr = [NSMutableString
//                        stringWithCapacity:([self length] * 2)];
//    
//    for(int i=0;i<[self length];i++)
//    {
//        NSString *newHexStr = [NSString stringWithFormat:@"%x",bytes[i]&0xff];///16进制数
//        if([newHexStr length]==1)
//            hexStr = [NSString stringWithFormat:@"%@0%@",hexStr,newHexStr];
//        else 
//            hexStr = [NSString stringWithFormat:@"%@%@",hexStr,newHexStr];
//    }
//    PXLog(@"bytes 的16进制数为:%@",hexStr);
//    //  return hexStr;
//    return [[hexStr copy] autorelease];
//}

//Byte数组－>16进制数
- (NSString *)stringWithBytes{
    Byte *bytes = (Byte *)[self bytes];
 //   NSString *hexStr=@"";
    NSMutableString *hexStr = [NSMutableString
                        stringWithCapacity:([self length] * 2)];

    for(int i=0;i<[self length];i++)
    {
        NSString *newHexStr = [NSString stringWithFormat:@"%X",bytes[i]&0xff];///16进制数
       
        if([newHexStr length]==1)
           // hexStr = [NSString stringWithFormat:@"%@0%@",hexStr,newHexStr];
        [hexStr appendFormat:@"0%@",newHexStr];
        else 
          //  hexStr = [NSString stringWithFormat:@"%@%@",hexStr,newHexStr];
        [hexStr appendFormat:@"%@",newHexStr];
    }
    NSLog(@"bytes 的16进制数为:%@",hexStr);
    return hexStr;
}

- (NSString*)stringWithHexBytes {
	NSMutableString *stringBuffer = [NSMutableString
                                     stringWithCapacity:([self length] * 2)];
	const unsigned char *dataBuffer = [self bytes];
	int i;
    
	for (i = 0; i < [self length]; ++i)
		[stringBuffer appendFormat:@"%lX", (unsigned long)dataBuffer[ i ]];
    
	return [stringBuffer copy];
}
+ (NSData *)desData:(NSData *)data key:(NSString *)keyString CCOperation:(CCOperation)op
{
    void *vkey = (void *)malloc(8);
    memcpy(vkey, [keyString UTF8String], 8);
    size_t bufferPtrSize = 0;
    bufferPtrSize = ([data length] + kCCBlockSizeDES) & ~(kCCBlockSizeDES - 1);
    
    char buffer [bufferPtrSize] ;
   
    memset(buffer, 0, sizeof(buffer));
    size_t bufferNumBytes;
    CCCryptorStatus cryptStatus = CCCrypt(op, 
                                          
                                          kCCAlgorithmDES, 
                                          
                                          kCCOptionPKCS7Padding | kCCOptionECBMode,
                                          
                                          [keyString UTF8String], 
                                          
                                          kCCKeySizeDES,
                                          
                                          vkey, 
                                          
                                          [data bytes], 
                                          
                                          [data length],
                                          
                                          buffer, 
                                          
                                          bufferPtrSize,
                                          
                                          &bufferNumBytes);
    free(vkey);
    if(cryptStatus == kCCSuccess)
    {
        NSData *returnData =  [NSData dataWithBytes:buffer length:bufferNumBytes];
        NSLog(@"return data is %@",returnData);
        return returnData;
        //        NSString *returnString = [[[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding]autorelease];
    }
    NSLog(@"des failed！cryptstatus is %i",cryptStatus);
    return nil;
    
}

- (Float64)extractFloat64AtOffset:(NSUInteger)offset {
    NSSwappedDouble swappedDouble;
    [self getBytes:&swappedDouble range:NSMakeRange(offset, sizeof(NSSwappedDouble))];
    return NSSwapBigDoubleToHost(swappedDouble);
}

- (UInt64)extractInt64FromBEAtOffset:(NSUInteger)offset {
    UInt64 beInt64;
    [self getBytes:&beInt64 range:NSMakeRange(offset, sizeof(UInt64))];
    return NSSwapBigLongLongToHost(beInt64);
}

- (UInt32)extractInt32FromBEAtOffset:(NSUInteger)offset {
    UInt32 beInt32;
    [self getBytes:&beInt32 range:NSMakeRange(offset, sizeof(UInt32))];
    return NSSwapBigIntToHost(beInt32);
}

- (UInt16)extractInt16FromBEAtOffset:(NSUInteger)offset {
    UInt16 beInt16;
    [self getBytes:&beInt16 range:NSMakeRange(offset, sizeof(UInt16))];
    return NSSwapBigShortToHost(beInt16);
}

@end
