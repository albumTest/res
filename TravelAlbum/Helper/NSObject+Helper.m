//
//  NSObject+Helper.m
//  paixiu
//
//  Created by darkdong on 12-8-10.
//  Copyright (c) 2012年 PaiXiu. All rights reserved.
//
#import <objc/runtime.h>
#import "NSObject+Helper.h"

static char kAssociatedObjectKey;

@implementation NSObject (Helper)

- (id)associatedObject {
    return objc_getAssociatedObject (self, &kAssociatedObjectKey);
}

- (void)setAssociatedObject:(id)associatedObject {
    objc_setAssociatedObject (self,
                              &kAssociatedObjectKey,
                              associatedObject,
                              OBJC_ASSOCIATION_RETAIN
                              );
}

@end
