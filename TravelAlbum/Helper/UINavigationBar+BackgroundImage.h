//
//  UINavigationBar+BackgroundImage.h
//  TravelAlbum
//
//  Created by Static Ga on 13-2-1.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationBar (BackgroundImage)
- (void)setBackGroundImage:(NSString *)imageName;
@end
