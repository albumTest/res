//
//  UINavigationController+Helper.h
//  Lepai
//
//  Created by 雪松 董 on 11-10-9.
//  Copyright 2011年 Lepai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Helper)

- (UIViewController*)viewControllerByClassName:(NSString *)vcClassName reverse:(BOOL)reverse;
- (UIViewController*)viewControllerByClass:(Class)viewControllerClass reverse:(BOOL)reverse;
- (void)removeViewControllerByClass:(Class)viewControllerClass;
- (BOOL)topViewControllerClassNameIsEqualTo:(NSString *)vcClassName;
@end
