//
//  UINavigationController+Helper.m
//  Lepai
//
//  Created by 雪松 董 on 11-10-9.
//  Copyright 2011年 Lepai. All rights reserved.
//

#import "UINavigationController+Helper.h"

@implementation UINavigationController (Helper)

- (UIViewController*)viewControllerByClassName:(NSString *)vcClassName reverse:(BOOL)reverse {
    Class vcClass = NSClassFromString(vcClassName);
    return [self viewControllerByClass:vcClass reverse:reverse];
}

- (UIViewController*)viewControllerByClass:(Class)viewControllerClass reverse:(BOOL)reverse {
    NSArray *viewControllers = self.viewControllers;
    if (reverse) {
        for (NSUInteger i = viewControllers.count; i != 0; --i) {
            UIViewController *viewController = [viewControllers objectAtIndex:i - 1];
            if ([viewController isKindOfClass:viewControllerClass]) {
                return viewController;
            }
        }
    }else {
        for (UIViewController *viewController in viewControllers) {
            if ([viewController isKindOfClass:viewControllerClass]) {
                return viewController;
            }
        }
    }
    return nil;
}

- (void)removeViewControllerByClass:(Class)viewControllerClass {
    NSMutableArray *marray = [NSMutableArray arrayWithArray:self.viewControllers];
    NSIndexSet *indexes = [marray indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return [obj isKindOfClass:viewControllerClass];
    }];
    if (indexes.count) {
        [marray removeObjectsAtIndexes:indexes];
        self.viewControllers = marray;
    }
}

- (BOOL)topViewControllerClassNameIsEqualTo:(NSString *)vcClassName {
    return [self.topViewController isKindOfClass:NSClassFromString(vcClassName)];
}

@end
