//
//  UINavigationBar+BackgroundImage.m
//  TravelAlbum
//
//  Created by Static Ga on 13-2-1.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import "UINavigationBar+BackgroundImage.h"

@implementation UINavigationBar (BackgroundImage)

- (void)setBackGroundImage:(NSString *)imageName {
    if ([[UINavigationBar class]respondsToSelector:@selector(appearance)]) {
        [self setBackgroundImage:[UIImage imageNamed:imageName] forBarMetrics:UIBarMetricsDefault];
    }else {
        UIImage *image = [UIImage imageNamed: imageName];
        [image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
//        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:imageName] forBarMetrics:UIBarMetricsDefault];
    }
}

@end
