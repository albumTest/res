//
//  AppDelegate.m
//  TravelAlbum
//
//  Created by rumble on 13-1-27.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import "TAAppDelegate.h"
#import "TATool.h"
#import "TAUtility.h"
#import "TAConstantDefine.h"
#import "TAUtility.h"
#import "FMDatabase.h"
#import "MobClick.h"
@implementation TAAppDelegate

- (void)dealloc
{
    NSLog(@"TAAppDelegate dealloc");
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
//    mapManager = [[BMKMapManager alloc] init];
//    [mapManager start:@"5e8974b8ecbb13bcd82be41a04a4534d" generalDelegate:nil];
    
    [MobClick startWithAppkey:@"5107806d527015505e000069"];
    
    self.cityArray = [[NSMutableArray alloc] init];
    monitorObjectLock_ = [[NSRecursiveLock alloc] init];
    monitoredObjects_ = [[NSMutableDictionary alloc] init];
    
    _coord2CityidArray = [[NSMutableArray alloc] init];
    _pointInPolygonArray = [[NSMutableArray alloc] init];
    _cityid2CityNameArray = [[NSMutableArray alloc] init];
    _cityid2NameMapDic = [[NSMutableDictionary alloc] init];
    _cityPolygonDic = [[NSMutableDictionary alloc] init];
    _locationTocityIdDic = [[NSMutableDictionary alloc] init];
    _nearbyLocationToCityMap = [[NSMutableDictionary alloc] init];
    _uncategorizedArray = [[NSMutableArray alloc] init];
    _assetURL2assetDic = [[NSMutableDictionary alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(assetsLibraryChanged:) name:ALAssetsLibraryChangedNotification object:nil];
    NSString *countryCode = [[NSLocale currentLocale] objectForKey: NSLocaleCountryCode];
    NSLog(@"countryCode is %@", countryCode);
    if ([countryCode isEqualToString:@"CN"]) {
        self.isChina = YES;
    }

    dispatch_async([TATool dbQueue], ^{
        UnzipDBFile();
    });
#ifdef Create_UserDB
    dispatch_async([TATool saveToUserDBQueue], ^{
        CreateUserDB();
    });
#endif
    
    return YES;
}

- (void)assetsLibraryChanged:(NSNotification *)notifi {
    if (_isEnumerateAllPhotos) {
        _isEnterForeground = YES;
    } else {
        _isEnterForeground = NO;
        EnumerateAllPhotosToDB();
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    NSLog(@"applicationWillEnterForeground");
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    if (self.isLocationServerDown) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationReEnumerateAllPhotos object:nil userInfo:nil];
    }
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)monitoredObjIsAlive:(NSUInteger)hashcode {
    [monitorObjectLock_ lock];
    BOOL isAlive = 0 == hashcode? YES: [[monitoredObjects_ objectForKey:[NSNumber numberWithUnsignedInteger:hashcode]] boolValue];
    [monitorObjectLock_ unlock];
    return isAlive;
}

- (void)addObjToMonitor:(id)obj {
    [monitorObjectLock_ lock];
    NSUInteger hashcode = [obj hash];
    [monitoredObjects_ setObject:[NSNumber numberWithBool:YES] forKey:[NSNumber numberWithUnsignedInteger:hashcode]];
    [monitorObjectLock_ unlock];
}

- (void)removeObjFromMonitor:(id)obj {
    [monitorObjectLock_ lock];
    NSUInteger hashcode = [obj hash];
    [monitoredObjects_ removeObjectForKey:[NSNumber numberWithUnsignedInteger:hashcode]];
    [monitorObjectLock_ unlock];
}


@end
