//
//  TACityViewController.h
//  TravelAlbum
//
//  Created by rumble on 13-1-27.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TACityViewController : UIViewController

@property (nonatomic, strong) IBOutlet UITableView *cityTableView;
@property (nonatomic, strong) NSMutableArray *cityTableArray;
@property (nonatomic, retain) NSRecursiveLock *myModelsLock;
@property (nonatomic, strong) UIView *tipsCell;
@end
