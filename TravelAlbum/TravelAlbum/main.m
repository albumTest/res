//
//  main.m
//  TravelAlbum
//
//  Created by rumble on 13-2-19.
//  Copyright (c) 2013年 PaiXiu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TAAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, @"TAAppDelegate");
    }
}
