//
//  AppDelegate.h
//  TravelAlbum
//
//  Created by rumble on 13-1-27.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import <UIKit/UIKit.h>

#define Create_UserDB
#define Enumerate_Photos

@interface TAAppDelegate : UIResponder <UIApplicationDelegate>
{
    NSRecursiveLock *monitorObjectLock_;
    NSMutableDictionary *monitoredObjects_;
}
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSMutableArray *cityArray;
@property (strong, nonatomic) NSMutableArray *coord2CityidArray;
@property (strong, nonatomic) NSMutableArray *pointInPolygonArray;
@property (strong, nonatomic) NSMutableArray *cityid2CityNameArray;
@property (strong, nonatomic) NSMutableDictionary *cityid2NameMapDic;
@property (strong, nonatomic) NSMutableDictionary *cityPolygonDic;
@property (strong, nonatomic) NSMutableDictionary *locationTocityIdDic;
@property (strong, nonatomic) NSMutableDictionary *nearbyLocationToCityMap;
@property (strong, nonatomic) NSMutableArray *uncategorizedArray;
@property (strong, nonatomic) NSMutableDictionary *assetURL2assetDic;
@property (nonatomic) BOOL isEnterForeground;
@property (nonatomic) BOOL isEnumerateAllPhotos;
@property (nonatomic) BOOL isChina;
@property (nonatomic) BOOL isLocationServerDown;
- (BOOL)monitoredObjIsAlive:(NSUInteger)hashcode;
- (void)addObjToMonitor:(id)obj;
- (void)removeObjFromMonitor:(id)obj;
@end
