//
//  FirstViewController.m
//  TravelAlbum
//
//  Created by rumble on 13-1-27.
//  Copyright (c) 2013年 paixiu. All rights reserved.
//

#import "TACityViewController.h"
#import "TAListCell.h"
#import "TAConstantDefine.h"
#import "TAListItem.h"
#import "TATool.h"
#import "TAUtility.h"
#import "TAThumbsViewController.h"
#import "UINavigationBar+BackgroundImage.h"
#import "TAAppDelegate.h"
#import "FMDatabase.h"
#import "UIView+Helper.h"
#import "TAThumbsTableViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "UIAlertView+Blocks.h"

#define kBarImageTag 1020
#define kBarButtonTag 1021
extern const CGFloat cellHeight;
@interface TACityViewController ()

@end
static NSInteger kCityRefreshCount = 10;
static NSInteger kCityRefreshNum = 0;
@implementation TACityViewController

@synthesize cityTableView;
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    NSLog(@"view will appear");
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque animated:YES];
    [self.navigationController.navigationBar setBackGroundImage:@"bg_top"];
    
    [self setBarImageTitle:YES];
    
    UINavigationBar *navbar = [[self navigationController] navigationBar];
    [navbar setTranslucent:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //    [self setBarImageTitle];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self hidBarImage];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    NSLog(@"firstViewController viewDidLoad");
    
    
    self.cityTableView.backgroundColor = RGBCOLOR(245, 245, 245);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addPhoto:) name:kNotificationGetCityName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deletePhoto:) name:kNotificationDeletePhotoCity object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enumerateAllPhoto:) name:kNotificationEnumerateAllPhotos object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showErrorView:) name:kNotificationReadPhotoFailed object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeErrorView:) name:kNotificationReadSuccess object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshFromDB:) name:kNotificationRefreshUIFromDB object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshAll) name:kNotificationReEnumerateAllPhotos object:nil];
    self.myModelsLock = [[NSRecursiveLock alloc] init];
    self.cityTableView.rowHeight = cellHeight;
    self.cityTableArray = [NSMutableArray array];
#ifdef Enumerate_Photos
    self.cityTableView.tableFooterView = [self showTipsCell:1];
    [self.cityTableView reloadData];
    dispatch_async([TATool cityQueue], ^{
        [self refreshFromDB:nil];
        EnumerateAllPhotosToDB();
    });
#endif
    
}

-(void)refreshAll {
    self.cityTableView.tableFooterView = [self showTipsCell:1];
    [self.cityTableView reloadData];
    dispatch_async([TATool cityQueue], ^{
        [self refreshFromDB:nil];
        EnumerateAllPhotosToDB();
    });
}

- (void)refreshFromDB:(NSNotification *)notifi {
    NSLog(@"refresh UI");
    if (notifi && notifi.object) {
        self.cityTableView.tableFooterView = nil;
    }
    FMDatabase *userDB = [TATool sharedUserDB];
    [userDB open];
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM %@ ORDER BY %@ DESC", DBCity2AssetURLTable, DBphotoCreateDate];
    FMResultSet *rs = [userDB executeQuery:sql];
    NSMutableArray *cityArray = [NSMutableArray array];
    
    while ([rs next]) {
        NSURL *assetURL = [NSURL URLWithString:[rs objectForColumnName:DBPhotoAssetURL]];
        NSString *cityName = [rs objectForColumnName:DBCityName];
        if (![cityName isEqualToString:DBUncategorizedCityName]) {
            [cityArray addObject:cityName];
            [cityArray addObject:assetURL];
        }
     }
    [rs close];
    [userDB close];
    
    dispatch_async(TAGetGlobalQueue(), ^{
        NSMutableArray *theCityTableArray = [NSMutableArray array];
        if (0 == cityArray.count && !notifi) {
//            dispatch_async(TAGetMainQueue(), ^{
//                self.cityTableView.tableFooterView = [self showTipsCell:1];
//            });
        } else {
            for (int i = 0; i < cityArray.count; i++) {
                NSString *cityName = [cityArray objectAtIndex:i++];
                NSURL *assetURL = [cityArray objectAtIndex:i];
                
                BOOL isMatch = NO;
                for (TAListItem *item in theCityTableArray) {
                    if ([item.cityName isEqualToString:cityName]) {
                        [item.urlArray insertObject:assetURL atIndex:0];
                        //[item.urlArray addObject:assetURL];
                        //item.coverViewURL = assetURL;
                        item.photosCount++;
                        isMatch = YES;
                        break;
                    }
                }
                if (!isMatch) {
                    TAListItem *item = [TAListItem new];
                    item.photosCount = 1;
                    item.cityName = cityName;
                    item.urlArray = [NSMutableArray array];
                    [item.urlArray addObject:assetURL];
                    item.coverViewURL = assetURL;
                    [theCityTableArray addObject:item];
                }
            }
        }
        
        dispatch_async(TAGetMainQueue(), ^{
            [self.myModelsLock lock];
            self.cityTableArray = theCityTableArray;
            [self setBarImageTitle:NO];
            [self.cityTableView reloadData];
            [self.myModelsLock unlock];
            if (notifi && notifi.object && 0 == self.cityTableArray.count) {
                RIButtonItem *cancelBtn = [RIButtonItem itemWithLabel:@"Cancel"];
                RIButtonItem *OkBtn = [RIButtonItem itemWithLabel:@"OK"];
                UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:nil message:TALocalizable(@"No photos with location data") cancelButtonItem:cancelBtn otherButtonItems:OkBtn, nil];
                [alerView show];
                self.cityTableView.tableFooterView = [self showTipsCell:2];
                [self.cityTableView reloadData];
            }
        });
    });
}

#define kErrorViewTag 3011
- (void)showErrorView :(NSNotification *)aNot{
    [self.cityTableArray removeAllObjects];
    [self.cityTableView reloadData];
    UIView *errorViewContainer = [self.view viewWithTag:kErrorViewTag];
    if (!errorViewContainer) {
        errorViewContainer = [[UIView alloc] initWithFrame:self.view.bounds];
        errorViewContainer.backgroundColor = [UIColor whiteColor];
        errorViewContainer.tag = 3011;
        
        UILabel *label1 = [[UILabel alloc] init];
        label1.backgroundColor = [UIColor clearColor];
        //        label1.text = NSLocalizedStringFromTable(@"This app does not have access to your photos.",@"Localizable", nil);
        label1.text = NSLocalizedString(@"This app does not have access to your photos.", nil);
        label1.font = [UIFont systemFontOfSize:16];
        label1.textColor = [UIColor grayColor];
        label1.numberOfLines = 0;
        label1.lineBreakMode = UILineBreakModeWordWrap;
        label1.textAlignment = NSTextAlignmentCenter;
        label1.width = 300;
        [label1 sizeToFit];
        label1.centerX = errorViewContainer.width / 2;
        label1.top = 120;
        [errorViewContainer addSubview:label1];
        
        UILabel *label2 = [[UILabel alloc] init];
        label2.backgroundColor = [UIColor clearColor];
        label2.text = NSLocalizedString(@"You need to",nil);
        label2.font = [UIFont systemFontOfSize:14];
        label2.textColor = [UIColor grayColor];
        
        label2.numberOfLines = 0;
        label2.lineBreakMode = UILineBreakModeWordWrap;
        label2.width = 300;
        
        [label2 sizeToFit];
        label2.centerX = errorViewContainer.width / 2;
        label2.top = label1.bottom + 20;
        [errorViewContainer addSubview:label2];
        
        UILabel *label3 = [[UILabel alloc] init];
        label3.backgroundColor = [UIColor clearColor];
        NSString *info = nil;
        
        BOOL isIos6 = NO;
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 6.0) {
            isIos6 = YES;
            info = TALocalizable(@"perform the following operations:");
        }else {
            isIos6 = NO;
            info = TALocalizable(@"turn on the Location Service in iphone Settings.");
        }
        label3.text = info;
        label3.font = [UIFont systemFontOfSize:14];
        label3.textColor = [UIColor grayColor];
        
        label3.numberOfLines = 0;
        label3.lineBreakMode = UILineBreakModeWordWrap;
        label3.width = 300;
        
        [label3 sizeToFit];
        label3.centerX = errorViewContainer.width / 2;
        label3.top = label2.bottom + 6;
        if (isIos6) {
            UIImageView *icon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ico_set"]];
            [icon sizeToFit];
            icon.left = label3.left;
            icon.top = label3.bottom + 6;
            [errorViewContainer addSubview:icon];
            
            UILabel *label4 = [[UILabel alloc] init];
            label4.backgroundColor = [UIColor clearColor];
            info = TALocalizable(@"1. Open iPhone Settings \n 2. Tap Privacy -> Photos \n 3. Set \"Travel Photos\" to ON");
            label4.numberOfLines = 0;
            label4.font = [UIFont systemFontOfSize:14];
            label4.textColor = [UIColor grayColor];
            label4.lineBreakMode = NSLineBreakByWordWrapping;
            label4.text = info;
            [label4 sizeToFit];
            
            label4.width = 300;
            label4.left = icon.right + 10;
            label4.centerY = icon.centerY;
            
            [errorViewContainer addSubview:label4];
        }
        [errorViewContainer addSubview:label3];
        [self.view addSubview:errorViewContainer];
    }
}

- (void)removeErrorView: (NSNotification *)aNot {
    UIView *errorView = [self.view viewWithTag:kErrorViewTag];
    if (errorView) {
        [errorView removeFromSuperview];
        errorView = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    NSLog(@"firstViewController dealloc");
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationGetCityName
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationDeletePhotoCity
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationEnumerateAllPhotos
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationReadPhotoFailed
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationRefreshUIFromDB
                                                  object:nil];
}

#pragma mark-
#pragma mark-UITableViewDelegate UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CityCell";
    TAListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(!cell){
        cell = [[TAListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        NSLog(@"indexPathRow %i result %i",indexPath.row,indexPath.row%2);
    }
    if (0 == indexPath.row % 2) {
        cell.contentView.backgroundColor = RGBCOLOR(245, 245, 245);
    }else {
        cell.contentView.backgroundColor = RGBCOLOR(230, 230, 230);
    }
    
    TAListItem *item = [self.cityTableArray objectAtIndex:indexPath.row];
    [cell setCellwithModel:item];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.cityTableArray.count;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TAListItem *item = [self.cityTableArray objectAtIndex:indexPath.row];
    
    TAThumbsTableViewController *cc = [[TAThumbsTableViewController alloc] initWithThumbs:item.urlArray title:item.cityName];
    [self.navigationController pushViewController:cc animated:YES];
    [self hidBarImage];
}


- (void)enumerateAllPhoto:(NSNotification *)notifi {
#ifdef Enumerate_Photos
    EnumerateAllPhotosToDB();
#endif
}

- (void)deletePhotoWithCityName:(NSString *)cityName assetURL:(NSURL *)assetURL {
    TAListItem *deletedItem = nil;
    if ([cityName isEqualToString:DBUncategorizedCityName]) {
        return;
    }
    NSLog(@"deletePhotoWithCityName %@ %@", cityName, assetURL.absoluteString);
    for (TAListItem *item in self.cityTableArray) {
        if ([item.cityName isEqualToString:cityName]) {
            [item.urlArray removeObject:assetURL];
            item.photosCount--;
            if (0 == item.photosCount) {
                deletedItem = item;
            } else {
                item.coverViewURL = [item.urlArray objectAtIndex:0];
            }
            break;
        }
    }
    if (deletedItem) {
        [self.myModelsLock lock];
        [self.cityTableArray removeObject:deletedItem];
        [self.myModelsLock unlock];
        kCityRefreshNum = kCityRefreshCount;
    }
    kCityRefreshNum++;
    if (kCityRefreshNum >= kCityRefreshCount) {
        kCityRefreshNum = 0;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setBarImageTitle:NO];
            [self.myModelsLock lock];
            [self.cityTableView reloadData];
            [self.myModelsLock unlock];
        });
    }
}
- (void)deletePhoto:(NSNotification *)notifi {
    //NSLog(@"deletePhoto");
    
    NSDictionary *dictionary = notifi.object;
    NSString *cityName = [dictionary objectForKey:DBCityName];
    NSURL *assetURL = [NSURL URLWithString:[dictionary objectForKey:DBPhotoAssetURL]];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self deletePhotoWithCityName:cityName assetURL:assetURL];
    });
}

- (void)addphotoWithCityName:(NSString *)cityName AssetURL:(NSURL *)assetURL{
    BOOL isMatch = NO;
    for (TAListItem *item in self.cityTableArray) {
        if ([item.cityName isEqualToString:cityName]) {
            [item.urlArray addObject:assetURL];
            item.photosCount++;
            isMatch = YES;
            break;
        }
    }
    if (!isMatch) {
        TAListItem *item = [TAListItem new];
        item.photosCount = 1;
        item.cityName = cityName;
        item.urlArray = [NSMutableArray array];
        [item.urlArray addObject:assetURL];
        item.coverViewURL = assetURL;
        [self.myModelsLock lock];
        [self.cityTableArray addObject:item];
        [self.myModelsLock unlock];
    }
    kCityRefreshNum++;
    if (kCityRefreshNum >= kCityRefreshCount) {
        kCityRefreshNum = 0;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setBarImageTitle:NO];
            [self.myModelsLock lock];
            [self.cityTableView reloadData];
            [self.myModelsLock unlock];
        });
    }
    
}
-(void)addPhoto:(NSNotification *)notifi {
    NSDictionary *dictionary = notifi.object;
    
    if (!dictionary) {
        return;
    }
    NSString *cityName = [dictionary objectForKey:@"cityName"];
    if ([cityName isEqualToString:DBUncategorizedCityName]) {
        return;
    }
    NSURL *assetURL = [NSURL URLWithString:[dictionary objectForKey:@"assetURL"]];
    //    dispatch_async([TATool getCityQueue], ^{
    //        [self addphotoWithCityName:cityName AssetURL:assetURL];
    //    });
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self addphotoWithCityName:cityName AssetURL:assetURL];
    });
    
}


- (void)setBarImageTitle :(BOOL)animated{
    //NSLog(@"topViewCOntrooler %@",self.navigationController.topViewController);
    UIViewController *topController = self.navigationController.topViewController;
    if (![topController isKindOfClass:[TACityViewController class]]) {
        return;
    }
    UIView *barImage = [self.navigationController.navigationBar viewWithTag:kBarImageTag];
    if (!barImage) {
        barImage = [TATool makeBarTitleImage:TALocalizable(@"ico_title_en") numberCount:self.cityTableArray.count];
        UIButton *button = (UIButton *)[barImage viewWithTag:kBarButtonTag];
        if (button) {
            [button setTitle:[NSString stringWithFormat:@"%i",self.cityTableArray.count] forState:UIControlStateNormal];
            button.titleLabel.font = [UIFont systemFontOfSize:12];
            [button setTitleColor:RGBCOLOR(130, 130, 130) forState:UIControlStateNormal];
            [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0.0, 0.0)];
            [button sizeToFit];
        }
    }else {
        if (animated) {
            barImage.alpha = 0.0;
        }else {
            barImage.alpha = 1.0;
        }
        barImage.hidden = NO;
        UIButton *button = (UIButton *)[barImage viewWithTag:kBarButtonTag];
        if (button) {
            [button setTitle:[NSString stringWithFormat:@"%i",self.cityTableArray.count] forState:UIControlStateNormal];
            button.titleLabel.font = [UIFont systemFontOfSize:12];
            [button setTitleColor:RGBCOLOR(130, 130, 130) forState:UIControlStateNormal];
            [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0.0, 0.0)];
            [button sizeToFit];
        }
    }
    barImage.height = self.navigationController.navigationBar.height;
    barImage.tag = kBarImageTag;
    [self.navigationController.navigationBar addSubview:barImage];
    if (animated) {
        [UIView animateWithDuration:0.3 delay:0.0 options:0 animations:^{
            barImage.alpha = 1.0;
        }completion:^(BOOL finished){
            
        }];
    }
}

- (void)hidBarImage {
    UIView *barImage = [self.navigationController.navigationBar viewWithTag:kBarImageTag];
    if (barImage) {
        [UIView animateWithDuration:0.2 delay:0.0 options:0 animations:^{
            barImage.alpha = 0.0;
        }completion:^(BOOL finished) {
            barImage.hidden = YES;
        }];
    }
}
- (void)viewDidUnload {
    [super viewDidUnload];
}

//- (BOOL)shouldAutorotate {
//    NSLog(@"shouldAutorotate");
//    return YES;
//}
//
//-(NSInteger)supportedInterfaceOrientations{
//    NSLog(@"supported ");
//    return  UIInterfaceOrientationMaskAll;
//}
- (UIView *)showTipsCell:(NSInteger)tipsNum {
    if (!_tipsCell) {
        _tipsCell = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 50)];
        _tipsCell.backgroundColor = RGBCOLOR(245, 245, 245);
        UILabel *tipsLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        tipsLab.backgroundColor = [UIColor clearColor];
        tipsLab.tag = 3;
        tipsLab.font = [UIFont systemFontOfSize:14];
        tipsLab.textColor = RGBCOLOR(160, 160, 160);
        tipsLab.textAlignment = UITextAlignmentCenter;

        UIActivityIndicatorView *activityView_ = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityView_.tag = 2;
        [activityView_ startAnimating];
        activityView_.frame = CGRectMake(0, 0, 30, 30);
        [_tipsCell addSubview:activityView_];
        [_tipsCell addSubview:tipsLab];
    }
    UIActivityIndicatorView *activityView_ = (UIActivityIndicatorView *)[_tipsCell viewWithTag:2];
    UILabel *tipsLab = (UILabel *)[_tipsCell viewWithTag:3];
       switch (tipsNum) {
        case 1:
            tipsLab.text = TALocalizable(@"Sorting your photos by location...");
            [tipsLab sizeToFit];
            NSLog(@"tipsLab frame is %@",NSStringFromCGRect(tipsLab.frame));
            tipsLab.center = CGPointMake(CGRectGetMidX(_tipsCell.bounds), CGRectGetMidY(_tipsCell.bounds));
            tipsLab.left +=activityView_.width/2;
            activityView_.centerY = CGRectGetMidY(tipsLab.frame);
            activityView_.right = tipsLab.left;
            activityView_.hidden = NO;
            
            break;
        case 2:
            tipsLab.text = TALocalizable(@"No photos with location data");
            [tipsLab sizeToFit];
            tipsLab.center = CGPointMake(CGRectGetMidX(_tipsCell.bounds), CGRectGetMidY(_tipsCell.bounds));
            activityView_.hidden = YES;
            break;
        default:
            break;
    }
 return _tipsCell;
}
@end
